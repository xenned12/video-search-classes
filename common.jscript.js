// Common Javascript and JQuery Functions 

(function($) {
	/*
	/* Class 	: JQUERY Function for Directories 
	/* Version  : 3.0 - 11/12/2015
	/* Subject  : Get the category to search from the active URL
	/* Author   : Bennex Louis Edquiba
	*/
	
	var map; var latlngs;
	var $hostname = window.location.hostname; //console.log($hostname);
	var $pathname = window.location.pathname; //console.log($pathname);
	var loaded = false;
	
	var isMobile = {    Android: function() {        return navigator.userAgent.match(/Android/i);    },    BlackBerry: function() {        return navigator.userAgent.match(/BlackBerry/i);    },    iOS: function() {        return navigator.userAgent.match(/iPhone|iPad|iPod/i);    },    Opera: function() {        return navigator.userAgent.match(/Opera Mini/i);    },    Windows: function() {        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);    },    any: function() {        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());    }};

	function getlistingnetwork(){
		var network_fragment = $pathname.split('/');
		var network = network_fragment.pop();// console.log(network);
		return network; 
	}
	
	function getsearchcategory(){
		var category_fragment = $pathname.split('/');
		var category = category_fragment[category_fragment.length -2];  //console.log(category);
		return category; 
	}
	
	function getcurrentmenu(){
		var urlsplit = $pathname.split('/');
		var menu = urlsplit.filter(function(e){return e}).shift(); //console.log(menu);
		return menu;
	}
	
	function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
	function createbreadcrumbs(){
		var _pathname = window.location.pathname;
		var pathnames = _pathname.replace("/Search/","").split('/');
		var breadcrumbs = '<div class="crumbs"><ul><li><a href="//'+ $hostname +'"> &equiv;</a></li>';
		var pathnamescount = pathnames.length;
		$.each(pathnames.slice(0,-1),function(index,value){
			var sliced = pathnames.slice(0,index);
			var joined = sliced.join('/');
			if(joined==""){
				breadcrumbs += '<li><a href="/Search/'+capitalizeFirstLetter(value)+'/1">'+decodeURIComponent(capitalizeFirstLetter(value)).replace(/\+/g, " ")+'</a></li>';
			}else{
				breadcrumbs += '<li><a href="/Search/'+capitalizeFirstLetter(joined)+'/'+capitalizeFirstLetter(value)+'/1">'+decodeURIComponent(capitalizeFirstLetter(value)).replace(/\+/g, " ")+'</a></li>';
			}
		})
		breadcrumbs += '</ul></div>';
		
		$(document).find('#search-results-listing').html(breadcrumbs).append('<hr />');
	}
	
	$.fn.paginate = function(pages){
		$(document).find(this).each(function(index, element) {
			$(this).html(pages.pagination);	
		});
	}
	
	$.fn.scroller = function(container){
		$(this).animate({
			scrollTop: $(container.name).offset().top - 200
		}, 1000);	
	}
	
	$.fn.removecontents = function(){
		$(this).each(function(index, element) {
        	$(this).remove();
		});	
	}
	
	$.fn.resultsloader = function(){
		$(this).append('<br/><h5 class="remove-loader"><i class="fa fa-spinner spinner"></i> Loading search results, please wait...</h5>');	
	}
	
	function searchArray(nameKey, myArray){
		for (var i=0; i < myArray.length; i++) {
			if (myArray[i].name === nameKey) {
				return myArray[i].value;
			}
		}
	}
	
	function strurlencode(str) {
  		str = (str + '')
    		.toString();
 
  		return encodeURIComponent(str)
			.replace(/!/g, '%21')
			.replace(/'/g, '%27')
			.replace(/\(/g, '%28')
			.replace(/\)/g, '%29')
			.replace(/\*/g, '%2A')
			.replace(/%20/g, '+');
	}
	/*
	var parts = window.location.hostname.split('.');
	var subdomain = parts.shift();
	var $hostname = parts.join('.');
	*/
	var $category = getsearchcategory();
	var $currentmenu = getcurrentmenu();
	var $network = getlistingnetwork();
	
	$('head').prepend('<link rel="stylesheet" href="/assets/common/css/remodal.css" />');
	$('head').prepend('<link rel="stylesheet" href="/assets/common/css/remodal-default-theme.css" />');
	$('head').prepend('<link rel="stylesheet" href="/assets/common/css/custom_plugin.css" />');
	$('head').prepend('<link rel="stylesheet" href="/assets/common/css/jquery.bxslider.css" />');
	$('head').prepend('<link rel="stylesheet" href="/assets/common/css/gridsystem.css" />');
	$('body').append('<script src="/assets/common/js/jquery.bxslider.js"></script>');
	$('body').append('<script src="/assets/common/js/jquery.slimscroll.min.js"></script>');
	//console.log('appended');
	
	$(document).on('closing', '[data-remodal-id=player]', function (e) {
		$('#player-container').html('<i class="fa fa-spinner fa-spin"></i> Please wait while we reload for the video content of this listing');
	});
	
	function detailspagecontent(){
		if($currentmenu == "Details"){ 
			var $owncompanyhtml;
			$.ajax({
				url:"/dircommon/commonclasses/ajax/listing.details",
				data: {'hostname':$hostname,'listingid':$category,'source':$network},
				type:"POST",
				dataType:"json",
				success: function(response){
					//console.log(response);
					$('body').prepend('<div class="remodal" data-remodal-id="owncompany"'+
					'data-remodal-options="hashTracking: false, closeOnOutsideClick: true">'+
					'<button data-remodal-action="close" class="remodal-close"></button>'+
					'<h1><strong>Own this Company</strong></h1>'+
					'<p>Fill up the fields below to be reviewed by the team</p>'+
					'<div class="spacer-20"></div>'+ response.owncompany +
					'</div>');

					$('body').prepend('<div class="remodal" data-remodal-id="streetview"'+
					'data-remodal-options="hashTracking: false, closeOnOutsideClick: true">'+
					'<button data-remodal-action="close" class="remodal-close"></button>'+
					'<div class="map-wrapper">'+
					'<div id="map_canvas_wrapper">'+
					'<div id="map_street_view"></div>'+
					'</div></div>'+
					'<div class="spacer-20"></div>'+ 
					'</div>');	
					
					$('body').prepend('<div class="remodal" data-remodal-id="player"'+
					'data-remodal-options="hashTracking: false, closeOnOutsideClick: true">'+
					'<button data-remodal-action="close" class="remodal-close"></button>'+
					'<div id="player-container" class="pull-center"><i class="fa fa-spinner fa-spin"></i> Please wait while we look for the video content of this listing</div>'+
					'<div class="spacer-20"></div>'+ 
					'</div>');	
					
					$('body').prepend('<div class="remodal" data-remodal-id="review"'+
					'data-remodal-options="hashTracking: false, closeOnOutsideClick: true">'+
					'<button data-remodal-action="close" class="remodal-close"></button>'+
					'<h1><strong>Rate &amp Review</strong></h1>'+
					'<p>Submit your rating and review to this listing for future reference</p>'+
					'<div class="spacer-20"></div>'+ response.reviews +
					'</div>');
					
					$('body').prepend('<div class="remodal" data-remodal-id="register"'+
  					  'data-remodal-options="hashTracking: false, closeOnOutsideClick: false">'+
  					  '<button data-remodal-action="close" class="remodal-close"></button>'+
  					  '<h1><strong>Create Account</strong></h1>'+
					  '<p>Fill up the fields below with the appropriate and valid information</p>'+
					  '<div class="spacer-20"></div>'+ response.reguser +
					  '</div>');
					
					 $('body').prepend('<div class="remodal" data-remodal-id="login"'+
  					  'data-remodal-options="hashTracking: false, closeOnOutsideClick: true">'+
  					  '<button data-remodal-action="close" class="remodal-close"></button>'+
  					  '<h1><strong>Account Login</strong></h1>'+
					  '<p>Fill up the fields below with valid username &amp; password</p>'+
					  '<div class="spacer-20"></div>'+
  					  '<form id="directory-login-form">'+
					  '<div class="row">'+
  					  '<div class="col-sm-12 minimize-padding"><input type="text" placeholder="Username" title="Enter Username" name="username" class="remodal-input large" required /></div>'+
  					  '</div><div class="spacer-5"></div><div class="row">'+
					  '<div class="col-sm-12 minimize-padding"><input type="password" placeholder="Password" title="Enter Password" name="password" class="remodal-input large" required /></div>'+
  					  '</div><div class="spacer-15"></div>'+
					  '<div class="row">'+
					  '<div class="col-sm-12 minimize-padding pull-center" id="login-confirmation"></div>'+
					  '</div><div class="spacer-5"></div>'+
					  '<div class="row">'+
  					  '<div class="col-sm-12 minimize-padding"><button type="submit" class="remodal-btn large primary full-width">LOGIN</button></div>'+
  					  '</div>'+
					  '</form>'+
					  '<!--<div class="spacer-20"></div>'+
					  '<div class="spacer-20"></div>'+
					  '<div class="row">'+
  					  '<div class="col-sm-12 minimize-padding"><hr></div>'+
  					  '</div>'+
					  '<div class="spacer-20"></div>'+
					  '<div class="spacer-20"></div>'+
					  '<h1><strong>Social Media Login</strong></h1>'+
					  '<p>LOGIN WITH YOUR CONNECTED SOCIAL MEDIA ACCOUNT</p>'+
					  '<div class="spacer-5"></div>'+
					  '<div class="row">'+
					  '<div class="col-sm-12 minimize-padding pull-center">'+
					  '<button class="remodal-btn square soc-facebook"><i class="fa fa-facebook fa-2x"></i></button>'+
  					  '<button class="remodal-btn square soc-twitter"><i class="fa fa-twitter fa-2x"></i></button>'+
  					  '<button class="remodal-btn square soc-googleplus"><i class="fa fa-google-plus fa-2x"></i></button></div>'+
  					  '</div><div class="spacer-20"></div>-->'+
					  '</div>');
					
					$('body').append('<script src="/assets/common/js/remodal.js"></script>');
					/*$('body').prepend('<a data-remodal-target="register">Registration Modal</a>');
					$('body').prepend('<a data-remodal-target="login">Login Modal</a>');
					$('body').prepend('<a data-remodal-target="owncompany">Own Modal</a>');
					$('body').prepend('<a data-remodal-target="review">Review</a>');*/
				}
			})
		}else{
			
			$.ajax({
				url:"/assets/common/ajax/getregistration",
				data: {'hostname':$hostname,'listingid':$category},
				type:"POST",
				dataType:"html",
				success: function(response){
					$('body').prepend('<div class="remodal" data-remodal-id="register"'+
  					  'data-remodal-options="hashTracking: false, closeOnOutsideClick: false">'+
  					  '<button data-remodal-action="close" class="remodal-close"></button>'+
  					  '<h1><strong>Create Account</strong></h1>'+
					  '<p>Fill up the fields below with the appropriate and valid information</p>'+
					  '<div class="spacer-20"></div>'+ response +
					  '</div>');
					$('body').prepend('<div class="remodal" data-remodal-id="login"'+
  					  'data-remodal-options="hashTracking: false, closeOnOutsideClick: true">'+
  					  '<button data-remodal-action="close" class="remodal-close"></button>'+
  					  '<h1><strong>Account Login</strong></h1>'+
					  '<p>FILL UP THE FIELDS BELOW WITH PROPER INFORMATION</p>'+
					  '<div class="spacer-20"></div>'+
  					  '<form id="directory-login-form">'+
					  '<div class="row">'+
  					  '<div class="col-sm-12 minimize-padding"><input type="text" placeholder="Username" title="Enter Username" name="username" class="remodal-input large" required /></div>'+
  					  '</div><div class="spacer-5"></div><div class="row">'+
					  '<div class="col-sm-12 minimize-padding"><input type="password" placeholder="Password" title="Enter Password" name="password" class="remodal-input large" required /></div>'+
  					  '</div><div class="spacer-5"></div>'+
					  '<div class="row">'+
  					  '<div class="col-sm-12 minimize-padding"><button type="submit" class="remodal-btn large primary full-width">LOGIN</button></div>'+
  					  '</div>'+
					  '</form>'+
					  '<!--<div class="spacer-20"></div>'+
					  '<div class="spacer-20"></div>'+
					  '<hr/>'+
					  '<div class="spacer-20"></div>'+
					  '<div class="spacer-20"></div>'+
					  '<h1><strong>Social Media Login</strong></h1>'+
					  '<p>LOGIN WITH YOUR CONNECTED SOCIAL MEDIA ACCOUNT</p>'+
					  '<div class="spacer-5"></div>'+
					  '<div class="row">'+
					  '<div class="col-sm-12 minimize-padding pull-center">'+
					  '<button class="remodal-btn square soc-facebook"><i class="fa fa-facebook fa-2x"></i></button>'+
  					  '<button class="remodal-btn square soc-twitter"><i class="fa fa-twitter fa-2x"></i></button>'+
  					  '<button class="remodal-btn square soc-googleplus"><i class="fa fa-google-plus fa-2x"></i></button></div>'+
  					  '</div><div class="spacer-20"></div>-->'+
					  '</div>');
					  $('body').append('<script src="/assets/common/js/remodal.js"></script>');
				}
			})
		}
	}
	detailspagecontent();
	
	function getboundary(encode,zoom){
		$.ajax({
			url:"/dircommon/commonclasses/ajax/get.location",
			data: {'hostname':$hostname,'encode':encode},
			type:"POST",
			dataType:"html",
			success: function(response){
				$.ajax({
					url:"/dircommon/commonclasses/ajax/get.location",
					data: {'hostname':$hostname,'encode':'true'},
					type:"POST",
					dataType:"html",
					success: function(response){
						$("#map_canvas,#map").each(function() {
							$(this).removeData();
							$(this).goMap({
								maptype: 'ROADMAP',
								scrollwheel: false,
								navigationControl: false,
								address: String(response),
								zoom: parseInt(zoom)
							})
						})
					}
				})
			}
		})
	}
	
	function boundary(encode){
		$.ajax({
			url:"/dircommon/commonclasses/ajax/get.location",
			data: {'hostname':$hostname,'encode':encode},
			type:"POST",
			dataType:"html",
			success: function(response){
				return String(response);
			}
		})
	}

	//console.log( getsearchcategory() );
	//console.log( getcurrentmenu() );
	
	
	/*
	/* Function  : getlocalstorageitem 
	/* Version   : 3.0 - 11/12/2015
	/* Subject   : Get the local storage variables needed for the query
	/* Author    : Bennex Louis Edquiba
	/* Variables : {$var} - [ 'lat' | 'long' | 'geodate' ]
	*/
	function getlocalstorageitem( $var ){
		return localStorage.getItem( $var );
	}
	
	/*
	/* Function  : setlatlong 
	/* Version   : 3.0 - 11/12/2015
	/* Subject   : Set the latitude and longitude variable from the local storage
	/* Author    : Bennex Louis Edquiba
	/* Variables : {$var} - [ 'lat' | 'long' | 'geodate' ]
	*/

	var lat = localStorage.getItem('lat');
	var long = localStorage.getItem('long');
	var datestored = localStorage.getItem('geodate');
	var latlong = lat+':'+long || $('#area').val();
	
	if(datestored!=''){
		var daterecorded = new Date(datestored);
		var todaysDate = new Date();
		var dateFormat = new Date(todaysDate.getYear(),todaysDate.getMonth(),todaysDate.getDate());
		var dateInterval = $.trim(((dateFormat-daterecorded).toString()));
		//console.log( "DateIntervalCheck : "+ dateInterval.toString() );
	}

	if(dateInterval != '0'){
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition, showError, {enableHighAccuracy:true, timeout:3000, maximumAge:0});
		} else { 
			alert("Geolocation is not supported by this browser.");
		}
	}else{
		console.log( "Latitude:" + lat + " | Longitude:" + long);
		console.log( "PositionAdded:" + datestored);
	}
	
	function showPosition(position) {
		//Set the Date Today
		var dateGet = new Date();
		var today = new Date( dateGet.getYear(),dateGet.getMonth(),dateGet.getDate() );
		
		//Write a Log into the Browsers //console
		//console.log( "Latitude:" + position.coords.latitude + " Longitude:" + position.coords.longitude);
		//console.log( "Date:" + today);
		
		//Set localStorage Items
		localStorage.setItem('lat',position.coords.latitude);
		localStorage.setItem('long',position.coords.longitude);
		localStorage.setItem('geodate',today);
		
	}
	
	function showError(error)
    {
	  switch(error.code)
	  {
		case error.PERMISSION_DENIED:
		  if( !isMobile.any() ) alert("User denied the request for Geolocation.")
		  break;
		case error.POSITION_UNAVAILABLE:
		  if( !isMobile.any() ) alert("Location information is unavailable.")
		  break;
		case error.TIMEOUT:
		  if( !isMobile.any() ) alert("The request to get user location timed out.")
		  break;
		case error.UNKNOWN_ERROR:
		  if( !isMobile.any() ) alert("An unknown error occurred.")
		  break;
	  }
   	}
	
	function setlatlong(){
		var lat = getlocalstorageitem('lat');
		var long = getlocalstorageitem('long');
		
		var latlong = lat+':'+long || $(document).find('#area').val();
		return latlong;
	}
	
	if(lat.length > 0){
		$("#map_canvas,#map").each(function() {
			$(this).removeData();
			$(this).goMap({
				maptype: 'ROADMAP',
				scrollwheel: false,
				navigationControl: false,
				zoom: 4,
				latitude: lat, 
				longitude: long 
			})
		})
	}else{
		$("#map_canvas,#map").each(function() {
			$(this).removeData();
			$(this).goMap({
				maptype: 'ROADMAP',
				scrollwheel: false,
				navigationControl: false,
				zoom: 4,
				address: 'USA'
			})
		})
	}
	
	//Click pagination
	$(document).on('click','.icon',function(){
		var _category = $(this).attr('id');
		var _hostname = window.location.hostname; //console.log($hostname);

		$.ajax({
			url:'/dircommon/commonclasses/ajax/create.map.markers',
			data: {'categoryname':_category,'hostname':_hostname},
			type:"POST",
			dataType:"json",
			success: function(response){
				//console.log( response );
				
				$.each($.parseJSON(response.markers),function(index,value){
					//console.log(value);
					jQuery.goMap.createMarker({
						latitude: value.latitude,
						longitude: value.longitude,
						draggable: false,
						icon: value.icon,
						html: value.html,
						group: _category
					});
				});
				
				/* Hiding all the markers on the map */
				for (var i in $.goMap.markers) {
					if (this[i] !== 0) {
						$.goMap.showHideMarker(jQuery.goMap.markers[i], false);
					}
				}
				
				/* Revealing markers from the corresponding group */
				$.goMap.showHideMarkerByGroup(_category, true);	

				//$.goMap.fitBounds('markers',response.markersid); 
				$.goMap.fitBounds('visible'); 
			}
		})
		
	})
	
	$.fn.sidebarget = function(){
		var $sidebarlist = $(document).find(this);
		var $_hostname = window.location.hostname; //console.log($hostname);
		var $_pathname = window.location.pathname; //console.log($pathname);
		
		if($sidebarlist.length > 0){
			console.log('Sidebar Found!');
			$.ajax({
				url:"/dircommon/commonclasses/ajax/show.sidebar.subcategories",
				data: {'categoryname':$_pathname,'hostname':$_hostname},
				type:"POST",
				dataType:"html",
				beforeSend: function(){
					$sidebarlist.html('<li><i class="fa fa-search"></i> Looking for sub-categories...</li>');
				},
				success: function(response){
					//console.log(response);
					if($.trim(response).length > 0 || $.trim(response).toString() != ""){
						$sidebarlist.html(response);
					}else{
						$sidebarlist.html('<li class="clearfix"><a href="javascript:void(0)"> No sub-categories found </a><br/></li>');
					}
				}
			})
		}
	}
	$('#custom-list').sidebarget();
	
	var $categoriesarea = $(document).find('#categories-area');
	if($categoriesarea.length > 0){
		//console.log("FOUND CATEGORIES");
		$.ajax({
			url:"/dircommon/commonclasses/ajax/show.categories",
			data: {'hostname':$hostname},
			type:"POST",
			dataType:"html",
			beforeSend: function(){
				$categoriesarea.html('<h5><i class="fa fa-spinner spinner"></i> Loading categories list, please wait...</h5>');
			},
			success: function(response){
				//console.log(response);
				$categoriesarea.html(response);
			}
		})
	}
	
	var $subcategoriesarea = $(document).find('#subcategories-area');
	if($subcategoriesarea.length > 0){
		//console.log("FOUND SUBCATEGORIES");
		$.ajax({
			url:"/dircommon/commonclasses/ajax/show.subcategories",
			data: {'categoryid':$pathname,'hostname':$hostname},
			type:"POST",
			dataType:"html",
			beforeSend: function(){
				$subcategoriesarea.html('<h5><i class="fa fa-spinner spinner"></i> Loading sub-categories list, please wait...</h5>');
			},
			success: function(response){
				$subcategoriesarea.html(response);
			}
		})
	}

	var $listingresult = $(document).find('#search-results-listing');
	if($listingresult.length > 0){

		var hostname = $hostname;
		var categoryname = $pathname;
		
		$.ajax({
			url:"/dircommon/commonclasses/ajax/search.listing.result",
			data: {'hostname':hostname,'categoryname':categoryname},
			type:"POST",
			dataType:"json",
			beforeSend: function(){
				$listingresult.resultsloader();
			},
			success: function(response){
				
				console.log(response);

				if( $.trim(response.listings).toString() != "" || $.trim(response.listings).length > 0){
					
					$(".remove-loader").removecontents(); //Remove the loading page contents
					createbreadcrumbs(); //Create a breadcrumps at the top of page
					$('#custom-list').sidebarget(); //Create the sidebar subcontents
					$listingresult.append(response.listings); //Append the listings result search formatted
					$('.'+response.paginationContainer).paginate({'pagination':response.pagination}); //Create a pagination up and down the results
					$('html, body').scroller({'name':'#search-results-listing'}); //Scroll down to results area on document ready
					
					if( $.trim(response.addbutton) != "" || $.trim(response.addbutton).length > 0 ){
						$(document).find('div.sidebar-content').each(function(index, element) {
							$(this).prepend(response.addbutton);
						});
					}else{
						$(document).find('.add-listing-tocategory').remove();
					}
					
					if(response.location == "USA"){
						$("#map_canvas,#map").each(function() {
							$(this).removeData();
							$(this).goMap({
								maptype: 'ROADMAP',
								scrollwheel: false,
								navigationControl: false,
								zoom: 8,
								markers: JSON.parse(response.markers)
							})
							
							$.goMap.createMarker({  
								latitude: lat, 
								longitude: long, 
								draggable: false,
								icon: '/assets/common/images/maps/point-main.png',
								html: { 
									content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
								},
								id: 'user-marker'
							}); 

							$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible'); 
						})	  
					}else{
						$("#map_canvas,#map").each(function() {
							$(this).removeData();
							$(this).goMap({
								maptype: 'ROADMAP',
								scrollwheel: false,
								navigationControl: false,
								zoom: 8,
								markers: JSON.parse(response.markers)
							})
							
							$.goMap.createMarker({  
								latitude: lat, 
								longitude: long, 
								draggable: false,
								icon: '/assets/common/images/maps/point-main.png',
								html: { 
									content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
								},
								id: 'user-marker'
							}); 

							$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible'); 
						})
					}
				}else if($.trim(response.category).toString() == "" || $.trim(response.category).toString().length <= 0){

					$(".remove-loader").removecontents(); //Remove the loading page contents
					createbreadcrumbs(); //Create a breadcrumps at the top of page
					$('#custom-list').sidebarget(); //Create the sidebar subcontents
					$listingresult.append('<div class="state-error"><p><strong>No search item entered!</strong> Please enter a company name or choose a category on the dropdown menu in order for us to show you the directory list.</p></div>'); //Append the listings result search formatted
					$('.'+response.paginationContainer).paginate({'pagination':''}); //Create a pagination up and down the results
					$('html, body').scroller({'name':'#search-results-listing'}); //Scroll down to results area on document ready
					
					var bound = String( boundary(true) );
					if( bound == 'USA'){
						getboundary(true,4);
					}else{
						getboundary(true,12);
					}
				}else{
					var bound = String( boundary(true) );
					if( bound == 'USA'){
						getboundary(true,4);
					}else{
						getboundary(true,12);
					}
					
					$(".remove-loader").removecontents(); //Remove the loading page contents
					createbreadcrumbs(); //Create a breadcrumps at the top of page
					$('#custom-list').sidebarget(); //Create the sidebar subcontents
					$listingresult.append('<div class="state-error"><p><strong>No Records Found!</strong> The item that you are trying to search didn\'t match any records. Please try another item.</p></div>');
					$('.'+response.paginationContainer).paginate({'pagination':''}); //Create a pagination up and down the results
					$('html, body').scroller({'name':'#search-results-listing'}); //Scroll down to results area on document ready
				}
				
				
			}
		})
	}else{
		$("#map_canvas,#map").each(function() {
			$(this).removeData();
		})
	}

	$(document).on('click','.direction-container-button',function(){
		$('#hide-show-button').text('Show Directions');
		$(document).find('.map-direction-container').hide("slide", { direction: "right" }, 1000);	
		return false;
	})
	
	var $listingdetails = $(document).find('#listing-details-container');
	if($listingdetails.length > 0){
		var listingid = $category; //console.log($category);
		var source = $network; //console.log($network);
		var hostname = $hostname; //console.log($hostname);
		
		$.ajax({
			url:"/dircommon/commonclasses/ajax/listing.details",
			data: {'hostname':hostname,'listingid':listingid,'source':source},
			type:"POST",
			dataType:"json",
			success: function(response){
				console.log(response);
				
				$listingdetails.html( response.details );

				var $companycanvas = $(document).find('#company_map_canvas');
				if( $companycanvas.length > 0 ){
					$companycanvas.removeData();
					$companycanvas.each(function() {
						$(this).goMap({
							maptype: 'ROADMAP',
							scrollwheel: false,
							navigationControl: false,
							zoom: 15,
							markers: JSON.parse(response.marker)
						})
					})	
				} //End of Company Canvas for Map
				
				var $companycanvas2 = $(document).find('#plugin_map_container');
				if( $companycanvas2.length > 0 ){
					$companycanvas2.removeData();
					
					$companycanvas2.each(function() {

						map = new GMaps({
						  div: '#plugin_map_container',
						  lat: lat,
						  lng: long,
						  streetViewControl: false,
						  mapTypeControl: false,
						  scrollwheel: false,
						  maptype: google.maps.MapTypeId.ROADMAP
						});

						map.addMarker({
						  lat: lat,
						  lng: long,
						  id: 'user-marker',
						  icon: '/assets/common/images/maps/point-main.png',
						  infoWindow: {
							  content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
							}
						});
						
						map.addMarker({
						  lat: response.latitude,
						  lng: response.longitude,
						  id: 'company-marker',
						  icon: '/assets/common/images/maps/point-b.png',
						  infoWindow: {
							  content: '<b>END POINT</b><br/><small>This is your destination.</small>'
							}
						});
						
						map.drawRoute({
						  origin: [lat, long],
						  destination: [response.latitude, response.longitude],
						  travelMode: 'driving',
						  strokeColor: '#232323',
						  strokeOpacity: 0.8,
						  strokeWeight: 6
						});
						
						map.addControl({
							position: 'top_left',
							content: 'Hide Directions',
							id: 'hide-show-button',
							style: {
								margin: '10px',
								padding: '4px 8px',
								border: 'solid 1px #E6E6E6',
								background: '#FFFFFF'
							},
							events: {
								click: function(){
									var element 		=	this;
									var button 			=	$(element).text();
									
									if( button != 'Hide Directions' ){
										$(element).text('Hide Directions');
										$(document).find('.map-direction-container').show("slide", { direction: "right" }, 1000);	
									}else{
										$(element).text('Show Directions');
										$(document).find('.map-direction-container').hide("slide", { direction: "right" }, 1000);
									}
									
								}
							}
						});
						
						map.addControl({
							position: 'top_left',
							content: 'Use Geolocation',
							id: 'use-geolocation-button',
							style: {
								margin: '10px',
								padding: '4px 8px',
								border: 'solid 1px #E6E6E6',
								background: '#FFFFFF'
							},
							events: {
								click: function(){
									var geo_button = this;
									
									navigator.geolocation.watchPosition(successCallback, errorCallback,{enableHighAccuracy:true,timeout:3000,maximumAge:0});
						
									function errorCallback() {
										alert('Please allow this page to monitor your geolocation to further assist you to your destination.')	
									}
									function successCallback(position) {
										$(geo_button).hide();
										
										map.removeMarkers();
										console.log(position.coords.latitude + ',' + position.coords.longitude);
										map.addMarker({
										  lat: position.coords.latitude,
										  lng: position.coords.longitude,
										  id: 'user-marker',
										  icon: '/assets/common/images/maps/point-main.png',
										  infoWindow: {
											  content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
											}
										});
										map.addMarker({
										  lat: response.latitude,//15.0180179,
										  lng: response.longitude,//120.5376041,
										  id: 'company-marker',
										  icon: '/assets/common/images/maps/point-b.png',
										  infoWindow: {
											  content: '<b>END POINT</b><br/><small>This is your destination.</small>'
											}
										});
									}
									
								}
							}
						});
						
						/**/
						var is_loaded_streetview = false
						$.ajax({
							url:"/assets/common/ajax/getlocation",
							data: {'hostname':$hostname,'listingid':$category,'source':$network},
							type:"POST",
							dataType:"json",
							success: function(response){	
								//console.log(response);			
								var $streetview = $(document).find('#bx_map_streetview');
								if( $streetview.length > 0 ){
									if(is_loaded_streetview == false){
										panorama = GMaps.createPanorama({
											el: '#bx_map_streetview',
											lat : response.latitude,
											lng : response.longitude,
										});
									}
								} //End of Company Street View
								
								is_loaded_streeview = true;
							}
						})
						
						$.ajax({
							url:"/dircommon/commonclasses/ajax/get.directions",
							data: {'origin':lat+','+long,'destination':response.latitude+','+response.longitude},
							type:"POST",
							dataType:"html",
							success: function(response){
								$(document).find('.map-direction-container').html(response);
								var $height = $(document).find('.map-direction-container').height();
								//$(document).find('.map-direction-container').slimScroll();
							}
						})
						
						latlngs = [{lat:lat, lng:long},{lat:response.latitude, lng:response.longitude}];
						var bounds = [];
						for (var i in latlngs) {
						  var latlng = new google.maps.LatLng(latlngs[i].lat, latlngs[i].lng);
						  bounds.push(latlng);
						}
						map.fitLatLngBounds(bounds);
						
						
					})
					
				} //End of Company Canvas for Map
				
				
				var $companystreetview = $(document).find('#company_map_canvas_street');
				if( $companystreetview.length > 0 ){
					panorama = GMaps.createPanorama({
						el: '#company_map_canvas_street',
						lat : response.latitude,
						lng : response.longitude,
					});
				} //End of Company Street View
				
				var $contactmapcanvasone = $(document).find('#contact_map_canvas_one');
				if( $contactmapcanvasone.length > 0 ){
					$contactmapcanvasone.removeData();
					$contactmapcanvasone.each(function() {
						$(this).goMap({
							maptype: 'ROADMAP',
							zoom: 13,
							scrollwheel: false,
							markers: JSON.parse(response.marker)
						});
					})
				}

				if(isMobile.any()){
					$('.map-control').find('a').trigger('click');
				}
			}
		})
	}
	
	
	$(document).on('click','.pagebtn',function(){
		var page = $(this).data('page');
		var location = $(this).data('location');
		var category = $category;
		
		//console.log('hostname'+$hostname+': categoryname'+category+': page'+page+': locationname'+location);
		$urlpathname = window.location.pathname;
		var categorylist = $urlpathname.replace('/Search','').split("/").filter(function(n){return n; }).slice(0,-1);
		var path = categorylist.join('/');
		window.history.pushState("", "", '/Search/' + path + '/' + page);
		
		$.ajax({
			url:"/dircommon/commonclasses/ajax/search.listing.result",
			data: {'hostname':$hostname,'categoryname':$urlpathname,'page':page,'locationname':location},
			type:"POST",
			dataType:"json",
			beforeSend: function(){
				$listingresult.html('<h5 class="remove-loader"><i class="fa fa-spinner spinner"></i> LOADING PAGE '+page+' OF SEARCH RESULTS, PLEASE WAIT...</h5>');
			},
			success: function(response){

				if( $.trim(response.listings).toString() != "" || $.trim(response.listings).length > 0){
					
					createbreadcrumbs();
					
					$listingresult.append(response.listings);
					
					$(document).find('.'+response.paginationContainer).each(function(index, element) {
						$(this).html(response.pagination);	
					});
					
					//console.log(response);
					
					if(response.location == "USA"){
						$("#map_canvas,#map").each(function() {
							$(this).removeData();
							$(this).goMap({
								maptype: 'ROADMAP',
								scrollwheel: false,
								navigationControl: false,
								zoom: 4,
								markers: JSON.parse(response.markers)
							})
						})
					}else{
						
						$("#map_canvas,#map").each(function() {
							$(this).removeData();
							$(this).goMap({
								maptype: 'ROADMAP',
								scrollwheel: false,
								navigationControl: false,
								zoom: 8,
								markers: JSON.parse(response.markers)
							})
						})
					}
				}else{
					$listingresult.html('<div class="state-error"><p><strong>No Records Found!</strong> The item that you are trying to search didn\'t match any records. Please try another item.</p></div>');
					$(document).find('.pagination').each(function(index, element) {
						$(this).html('');	
					});
					$('html, body').animate({
						scrollTop: $("#company").offset().top
					}, 1000);
					var bound = String( boundary(true) );
					if( bound == 'USA'){
						getboundary(true,4);
					}else{
						getboundary(true,12);
					}
					
				}

			}
		})
	})
	
	var $catdropdown = $(document).find('.category-name');
	var $locdropdown = $(document).find('.location-name');
	if($catdropdown.length > 0 && $locdropdown.length > 0){
		//console.log("FOUND DROPDOWNS");
		$.ajax({
			url:"/dircommon/commonclasses/ajax/create.dropdowns",
			data: {'hostname':$hostname},
			type:"POST",
			dataType:"json",
			beforeSend: function(){
				//console.log("Dropdown Processed!");
			},success: function(response){
				//console.log(response);
				$catdropdown.each(function(index, element) {
					$(this).html(response.dropdowncategories);
				});
				$locdropdown.each(function(index, element) {
					$(this).html(response.dropdownlocations);
				});
				$( '.select-box' ).each(function(){
					if( $(this).hasClass('dropdown-check') ){
						$(this).uouSelectBox();
					}
				});
			},error: function(){
				$( '.select-box' ).each(function(){
					if( $(this).hasClass('dropdown-check') ){
						$(this).uouSelectBox();
					}
				});
			},timeout: function(){
				$( '.select-box' ).each(function(){
					if( $(this).hasClass('dropdown-check') ){
						$(this).uouSelectBox();
					}
				});
			}
		})
	}
	
	$(document).on('submit','#default-search',function(){
		var defaultsearchdata = $(this).serialize()+'&hostname='+$hostname;
		var defaultarray = $(this).serializeArray();

		var inputcompany = searchArray('companyname',defaultarray);//defaultarray[1].value;
		var inputlocation = searchArray('locationname',defaultarray);//defaultarray[2].value;
		var inputcategory = searchArray('categoryname',defaultarray);//defaultarray[3].value;
		
		var currentmenu = getcurrentmenu();
		//console.log(defaultarray);

		if( $.trim(currentmenu) == 'Search'){
			
			if( $.trim(inputcompany).length > 0 ){
				window.history.pushState("", "", '/Search/'+ strurlencode(inputcompany) + '/1');
			}else{
				window.history.pushState("", "", '/Search/'+ strurlencode( inputcategory.charAt(0).toUpperCase() + inputcategory.substr(1) ) + '/1');
			}
			
			$.ajax({
				url:"/dircommon/commonclasses/ajax/search.listing.result",
				data: defaultsearchdata,
				type:"POST",
				dataType:"json",
				success: function(response){
					//console.log(response);
					if( $.trim(response.listings).toString() != "" || $.trim(response.listings).toString().length > 0){
						//console.log(response);
						createbreadcrumbs();
						$('#custom-list').sidebarget();
						$listingresult.append(response.listings);
						$('html, body').scroller({'name':'#search-results-listing'});
						$('.'+response.paginationContainer).paginate({'pagination':response.pagination});
						
						if( response.addbutton != false ){
							$(document).find('div.sidebar-content').each(function(index, element) {
								$(document).find('.add-listing-tocategory').remove();
								$(this).prepend(response.addbutton);
							});
						}else{
							$(document).find('.add-listing-tocategory').remove();
						}
						
						if(response.location == "USA"){
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 4,
									markers: JSON.parse(response.markers)
								})
								
								$.goMap.createMarker({  
									latitude: lat, 
									longitude: long, 
									draggable: false,
									icon: '/assets/common/images/maps/point-main.png',
									html: { 
										content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
									},
									id: 'user-marker'
								}); 
		
								$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible');
							})
						}else{
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 13,
									markers: JSON.parse(response.markers)
								})
								
								$.goMap.createMarker({  
									latitude: lat, 
									longitude: long, 
									draggable: false,
									icon: '/assets/common/images/maps/point-main.png',
									html: { 
										content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
									},
									id: 'user-marker'
								}); 
	
								$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible'); 
							})
						}
					}else if($.trim(response.category).toString() == "" || $.trim(response.category).toString().length <= 0){
						createbreadcrumbs();
						$('#custom-list').sidebarget();
						$listingresult.append('<div class="state-error"><p><strong>No search item entered!</strong> Please enter a company name or choose a category on the dropdown menu in order for us to show you the directory list.</p></div>');						
						$('html, body').scroller({'name':'#search-results-listing'});
						$('.'+response.paginationContainer).paginate({'pagination':''});
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
					}else{
						createbreadcrumbs();
						$('#custom-list').sidebarget();
						$listingresult.append('<div class="state-error"><p><strong>No Records Found!</strong> The item that you are trying to search didn\'t match any records. Please try another item.</p></div>');
						$('html, body').scroller({'name':'#search-results-listing'});
						$('.'+response.paginationContainer).paginate({'pagination':''});
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
					}
				}
			})
			
			$(document).find('#default-search')[0].reset();
			$(document).find('select').each(function(index, element) {
				$(this).find('option:selected').prop('selected',false);
			});
			
		}else{
			if( $.trim(inputcompany.length) > 0 ){
				window.location = '/Search/'+ strurlencode(inputcompany) + '/1';
			}else if( $.trim(inputlocation.length) <= 0 ){
				window.location = '/Search/'+ strurlencode(inputcategory.charAt(0).toUpperCase() + inputcategory.substr(1)) + '/1';
			}
			
			$(document).find('#default-search')[0].reset();
			$(document).find('select').each(function(index, element) {
				$(this).find('option:selected').prop('selected',false);
			});
		}

		return false;
	})
	
	$(document).on('submit','#advanced-search',function(){
		var advancedsearchdata = $(this).serialize()+'&hostname='+$hostname;
		var defaultarray = $(this).serializeArray();
		var inputcompany = searchArray('companyname',defaultarray);//defaultarray[1].value;
		var inputlocation = defaultarray[2].value;
		var inputcategory = searchArray('categoryname',defaultarray);//defaultarray[3].value;
		
		var currentmenu = getcurrentmenu();
		console.log(inputcompany);
	
		if( $.trim(currentmenu) == 'Search'){
			
			if( $.trim(inputcompany).length > 0 ){
				window.history.pushState("", "", '/Search/'+ strurlencode(inputcompany) + '/1');
			}else{
				window.history.pushState("", "", '/Search/'+ strurlencode(inputcategory.charAt(0).toUpperCase() + inputcategory.substr(1))+ '/1');
			}
			
			$.ajax({
				url:"/dircommon/commonclasses/ajax/search.listing.result",
				data: advancedsearchdata,
				type:"POST",
				dataType:"json",
				success: function(response){
					
					if( $.trim(response.listings).toString() != "" || $.trim(response.listings).length > 0){
						console.log(response);
						createbreadcrumbs();
						$('#custom-list').sidebarget();
						$listingresult.append(response.listings);
						
						$('html, body').scroller({'name':'#search-results-listing'});
						$('.'+response.paginationContainer).paginate({'pagination':response.pagination});
						
						if( response.addbutton != false ){
							$(document).find('div.sidebar-content').each(function(index, element) {
								$(document).find('.add-listing-tocategory').remove();
								$(this).prepend(response.addbutton);
							});
						}else{
							$(document).find('.add-listing-tocategory').remove();
						}
						
						if(response.location == "USA"){
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 4,
									markers: JSON.parse(response.markers)
								})
								
								$.goMap.createMarker({  
									latitude: lat, 
									longitude: long, 
									draggable: false,
									icon: '/assets/common/images/maps/point-main.png',
									html: { 
										content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
									},
									id: 'user-marker'
								}); 
		
								$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible');
							})
						}else{
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 10,
									markers: JSON.parse(response.markers)
								})
							})
							
							$.goMap.createMarker({  
								latitude: lat, 
								longitude: long, 
								draggable: false,
								icon: '/assets/common/images/maps/point-main.png',
								html: { 
									content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
								},
								id: 'user-marker'
							}); 
	
							$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible'); 
						}
					}else if($.trim(response.category).toString() == "" || $.trim(response.category).toString().length <= 0){
						$listingresult.html('<div class="state-error"><p><strong>No search item entered!</strong> Please enter a company name or choose a category on the dropdown menu in order for us to show you the directory list.</p></div>');
						$(document).find('.pagination').each(function(index, element) {
							$(this).html('');	
						});
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
					}else{
						$listingresult.html('<div class="state-error"><p><strong>No Records Found!</strong> The item that you are trying to search didn\'t match any records. Please try another item.</p></div>');
						$(document).find('.pagination').each(function(index, element) {
							$(this).html('');	
						});
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
						
					}
				}
			})
			
			$(this)[0].reset();
		}else{
			if( $.trim(inputcompany.length) > 0 ){
				window.location = encodeURI('/Search/'+ inputcompany.replace(/\s+/g,'+')) + '/1';
			}else if( $.trim(inputlocation.length) <= 0 ){
				window.location = '/Search/'+ encodeURI(inputcategory.charAt(0).toUpperCase() + inputcategory.substr(1)) + '/1';
			}
			
			$(document).find('#advanced-search')[0].reset();
			$(document).find('select').each(function(index, element) {
				$(this).find('option:selected').prop('selected',false);
			});
		}
		
		return false;
	})
	
	$(document).on('submit','#quick-search',function(){
		var currentmenu = getcurrentmenu();
		var quicksearchdata = $(this).serialize()+'&hostname='+$hostname;
		var input = $(this).children('input[name="companyname"]').val();
		
		if( $.trim(currentmenu) == 'Search'){
			
			window.history.pushState("", "", '/Search/'+ strurlencode(input.charAt(0).toUpperCase() + input.substr(1)) + '/1');
			
			$.ajax({
				url:"/dircommon/commonclasses/ajax/search.listing.result",
				data: quicksearchdata,
				type:"POST",
				dataType:"json",
				success: function(response){
					//console.log(response);
					
					if( $.trim(response.listings).toString() != "" || $.trim(response.listings).length > 0){
						//console.log(response);
						$listingresult.html(response.listings);
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						
						$(document).find('.pagination').each(function(index, element) {
							$(this).html(response.pagination);	
						});
					
						if( response.addbutton != false ){
							$(document).find('div.sidebar-content').each(function(index, element) {
								$(document).find('.add-listing-tocategory').remove();
								$(this).prepend(response.addbutton);
							});
						}else{
							$(document).find('.add-listing-tocategory').remove();
						}
						
						if(response.location == "USA"){
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 4,
									markers: JSON.parse(response.markers)
								})
								
								$.goMap.createMarker({  
									latitude: lat, 
									longitude: long, 
									draggable: false,
									icon: '/assets/common/images/maps/point-main.png',
									html: { 
										content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
									},
									id: 'user-marker'
								}); 
		
								$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible');
							})
						}else{
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 10,
									markers: JSON.parse(response.markers)
								})
							})
							
							$.goMap.createMarker({  
								latitude: lat, 
								longitude: long, 
								draggable: false,
								icon: '/assets/common/images/maps/point-main.png',
								html: { 
									content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
								},
								id: 'user-marker'
							}); 
	
							$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible');
						}
					}else if($.trim(response.category).toString() == "" || $.trim(response.category).toString().length <= 0){
						$listingresult.html('<div class="state-error"><p><strong>No search item entered!</strong> Please enter a company name or choose a category on the dropdown menu in order for us to show you the directory list.</p></div>');
						$(document).find('.pagination').each(function(index, element) {
							$(this).html('');	
						});
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
					}else{
						$listingresult.html('<div class="state-error"><p><strong>No Records Found!</strong> The item that you are trying to search didn\'t match any records. Please try another item.</p></div>');
						$(document).find('.pagination').each(function(index, element) {
							$(this).html('');	
						});
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
						
					}
				}
			})
			
			$(this)[0].reset();
			
			createbreadcrumbs();
			
		}else{
			window.location.href = '/Search/' + encodeURI(input.charAt(0).toUpperCase() + input.substr(1)) + '/1';
		}
		
		return false;
	})
	
	$(document).on('submit','#quick-search-mobile',function(){
		var currentmenu = getcurrentmenu();
		var quicksearchdata = $(this).serialize()+'&hostname='+$hostname;
		var input = $(this).children('input[name="companyname"]').val();
		
		if( $.trim(currentmenu) == 'Search'){
			
			window.history.pushState("", "", '/Search/'+ strurlencode(input.charAt(0).toUpperCase() + input.substr(1)) + '/1');
			
			$.ajax({
				url:"/dircommon/commonclasses/ajax/search.listing.result",
				data: quicksearchdata,
				type:"POST",
				dataType:"json",
				success: function(response){
					//console.log(response);
					if( $.trim(response.listings).toString() != "" || $.trim(response.listings).length > 0){
						//console.log(response);
						$listingresult.html(response.listings);
						
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						
						$(document).find('.pagination').each(function(index, element) {
							$(this).html(response.pagination);	
						});
						
						if( response.addbutton != false ){
							$(document).find('div.sidebar-content').each(function(index, element) {
								$(document).find('.add-listing-tocategory').remove();
								$(this).prepend(response.addbutton);
							});
						}else{
							$(document).find('.add-listing-tocategory').remove();
						}
						
						if(response.location == "USA"){
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 4,
									markers: JSON.parse(response.markers)
								})
								
								$.goMap.createMarker({  
									latitude: lat, 
									longitude: long, 
									draggable: false,
									icon: '/assets/common/images/maps/point-main.png',
									html: { 
										content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
									},
									id: 'user-marker'
								}); 
		
								$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible');
							})
						}else{
							$("#map_canvas,#map").each(function() {
								$(this).removeData();
								$(this).goMap({
									maptype: 'ROADMAP',
									scrollwheel: false,
									navigationControl: false,
									zoom: 10,
									markers: JSON.parse(response.markers)
								})
								
								$.goMap.createMarker({  
									latitude: lat, 
									longitude: long, 
									draggable: false,
									icon: '/assets/common/images/maps/point-main.png',
									html: { 
										content: '<b>YOU ARE HERE</b><br/><small>This is your current location.</small>'
									},
									id: 'user-marker'
								}); 
		
								$.goMap.fitBounds('markers',response.markersid); $.goMap.fitBounds('visible');
							})
						}
					}else if($.trim(response.category).toString() == "" || $.trim(response.category).toString().length <= 0){
						$listingresult.html('<div class="state-error"><p><strong>No search item entered!</strong> Please enter a company name or choose a category on the dropdown menu in order for us to show you the directory list.</p></div>');
						$(document).find('.pagination').each(function(index, element) {
							$(this).html('');	
						});
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
					}else{
						$listingresult.html('<div class="state-error"><p><strong>No Records Found!</strong> The item that you are trying to search didn\'t match any records. Please try another item.</p></div>');
						$(document).find('.pagination').each(function(index, element) {
							$(this).html('');	
						});
						$('html, body').animate({
							scrollTop: $("#company").offset().top
						}, 1000);
						var bound = String( boundary(true) );
						if( bound == 'USA'){
							getboundary(true,4);
						}else{
							getboundary(true,12);
						}
						
					}
				}
			})
			
			$(this)[0].reset();
			
			createbreadcrumbs();
			
		}else{
			window.location.href = '/Search/' + encodeURI(input.charAt(0).toUpperCase() + input.substr(1)) + '/1';
		}
		
		return false;
	})
	
	//Own Company Form Area
	$(document).on('submit','#form_own_this_company',function(){
		var info = $(this).serialize();
		var reganswer = $(document).find('#reganswer').val();
	   
		if($('#verused').val()=='' || $('#videoready').val()==''){
			$('#warning').html('<div class="alert-box notice"><span><i class="fa fa-exclamation-triangle"></i> Please complete the information needed. Fields with asterisk (*) are required.</div>');
		}else{
			if( $(document).find('#humanreganswer').val() == '' ){
				$('#warning').html('<div class="alert-box notice"><span><i class="fa fa-exclamation-triangle"></i> Please answer the security question below to prove you are a human</div>');
			}else if( $(document).find('#humanreganswer').val() == reganswer ){
		   
				$.ajax({
					url:"/assets/common/ajax/owncompany",
					type:"POST",
					dataType:"html",
					data:info,
					beforeSend: function(){
						$('#warning').html('<div class="alert-box notice"><span><i class="fa fa-spinner spinner"></i> Loading, please wait...</div>'); 
					},
					success: function(response){
						//console.log(response);
						if(response=='YES'){
							$('#warning').html('<div class="alert-box notice"><span><i class="fa fa-check-square"></i> Yes! Information was added and will be verified by the team. Be sure that the main contact info for verification is attended and available. </div>');
							$('#company_profile_button').trigger('click');
							$('#form_own_this_company')[0].reset();
							
							$('#yes_container').hide();
							$('#other_hosting').hide();
							$('#no_container').hide();
						}else if(response=='NO'){
							$('#warning').html('<div class="alert-box notice"><span><i class="fa fa-exclamation-triangle"></i> Ooops! An error has occured while trying to save the info. Please try again later.  </div>');
						}else{
							$('#warning').html('<div class="alert-box notice"><span><i class="fa fa-exclamation-triangle"></i> Ooops! An error has occured while trying to save the info. Possible reason : the company has already been owned by someone or you owned a company already, please contact the administrator for further information about the account.  </div>');
						}
					}
				})
			}else{
				$('#warning').html('<div class="alert-box notice"><span><i class="fa fa-exclamation-triangle"></i> The answer to the security question seems not right. Please enter the right answer to the question.</div>');
			}
	   }
	   //console.log(info);
	   return false;
	})
   
	$(document).on('change','#videoready',function(){
	 var ready = $(this).val();
	 if(ready=="1"){
		 $('#yesno_container').css('display','block');
		 $('#yes_container').find('select option:eq(0)').prop('selected', true);
		 $('#yes_container').css('display','block');
		 $('#no_container').css('display','none');
		 $('#other_hosting').css('display','none');
		 $('#wehost_container').css('display','none');
	 }else if(ready=="0"){
		 $('#yesno_container').css('display','block');
		 $('#no_container').find('select option:eq(0)').prop('selected', true);
		 $('#no_container').css('display','block');
		 $('#yes_container').css('display','none');
		 $('#other_hosting').css('display','none');
		 $('#wehost_container').css('display','none');
	 }else{
		 $('#yesno_container').css('display','none');
		 $('#other_hosting').css('display','none');
		 $('#wehost_container').css('display','none');
	 }
	 
		//console.log(ready);
	})
   
	$(document).on('change','#hostingselect',function(){
		var host = $(this).val();
		if(host=="None"){
			$('#other_hosting').css('display','none');	
			$('#wehost_container').css('display','block');
			('#wehost_video').find('select option:eq(0)').prop('selected', true);
		}else if(host=="Other"){
			$('#wehost_container').css('display','none');
			$('#other_hosting').css('display','block');	
		}else{
			$('#other_hosting').css('display','none');
			$('#wehost_container').css('display','none');
		}
		
		//console.log(host);
	})
	
	$(document).on('submit','#directory-reguser-form',function(){
		var humananswer = $.trim( $('#humanusereganswer').val() );
		var answer = $('#usereganswer').val();
		var passconfirm = $('#confirm').val();
		var password = $('#password').val();
		var formdata = $(this).serialize();
		
		if( password == passconfirm ){
			if( humananswer == ""){
				$('#user-confirmation-registration').html('<i class="fa fa-exclamation-triangle"></i> Please answer the security question to prove that you are a human');
			}else if( answer == humananswer ){
				$.ajax({
					url:"/assets/common/ajax/registrationuser",
					data:formdata,
					dataType:"json",
					type:"POST",
					beforeSend: function(){
						$('#user-confirmation-registration').html('<i class="fa fa-cog fa-spin"></i> Processing registration. Please wait for a moment.');
					},
					success: function(output){
						$('#user-confirmation-registration').html(output.statement);
						if( output.status == "ok"){
							$('#directory-reguser-form')[0].reset();
							var first = Math.floor((Math.random() * 100) + 1); 
							var second = Math.floor((Math.random() * 100) + 1); 
							var sum = first + second;
							
							$html = '<div class="minimize-padding col-sm-5 remodal-input"><input type="hidden" id="usereganswer" value="'+sum+'" /><h3> '+first+' + '+second+'  = ? </h3> </div>'+
							'<div class="minimize-padding col-sm-7"><input type="text" required class="remodal-input" id="humanusereganswer" placeholder="Answer to Question" /></div>';
							
							$('#regusersecquestion').html($html);
						}
					}
				})

			}else{
				$('#user-confirmation-registration').html('<i class="fa fa-exclamation-triangle"></i> Please answer the security question correctly in order for us to process your registration');
			}
		}else{
			$('#user-confirmation-registration').html('<i class="fa fa-exclamation-triangle"></i> The password and confirm password didn\'t matched. Please check the password and enter again.');
		}
		
		return false;
	})
	
	$(document).on('submit','#directory-login-form',function(){
		var logindata = $(this).serialize();
		
		$.ajax({
			url:"/assets/common/ajax/loginuser",
			data:logindata,
			dataType:"json",
			type:"POST",
			beforeSend: function(){
				$('#login-confirmation').html('<i class="fa fa-spinner fa-spin"></i> Verifying account, please wait...');
			},
			success: function(login){
				//console.log( login );
				if(login.status=='ok'){
					$('#login-confirmation').html('<i class="fa fa-check-square"></i> Account verified! Please wait while we reload the page.');
					window.location = window.location.href;
				}else{
					$('#login-confirmation').html('<i class="fa fa-exclamation-triangle"></i> Username or password is not correct. Please verify entered data.');
				}
			}
		})
		
		return false;	
	})
	
	$(document).on('submit','#directory-ratereview-form',function(){
		var ratereview = $(this).serialize();
		
		$.ajax({
			url:"/assets/common/ajax/submitreview",
			dataType:"html",
			type:"POST",
			data:ratereview,
			beforeSend: function(){
				$('#review-confirmation').html('<i class="fa fa-spinner fa-spin"></i> Please wait while we post your review');
			},
			success: function(rate){
				$('#review-confirmation').html('<i class="fa fa-check-square"></i> Review and rating has been saved and will be posted');
				$('#directory-ratereview-form')[0].reset();
			}
		})
		
		return false;
	})
	
	$(document).find('a.log-me-out').each(function(index, element) {
		$(this).click(function(e) {
			//console.log('Logout Initialized');
			$.ajax({
				url:"/assets/common/ajax/logout",
				data: {'out':'1' },
				dataType:"html",
				type:"POST",
				success: function(){
					window.location = window.location.href;
				}
			})
		})
	});
	
//--------------------------------------------------------
  $(document).on('click','.button-content button',function(e) {
    //console.log('clickes');

    if ($(this).hasClass('general-view-btn')) {
      $(this).addClass('active')
      .siblings().removeClass('active')
      .parent().parent().next().css("left","0%");


    } else if($(this).hasClass('map-view-btn')) {
      $(this).addClass('active')
      .siblings().removeClass('active')
      .parent().parent().next().css("left","-100%");

    } else if($(this).hasClass('male-view-btn')) {
      $(this).addClass('active')
     .siblings().removeClass('active')
     .parent().parent().next().css("left","-200%");
    }

  });
  
  $(document).on('click','#streetview-show',function(){
	var $hostname = window.location.hostname;
	var $category = getsearchcategory();
	
	$.ajax({
		url:"/assets/common/ajax/getlocation",
		data: {'hostname':$hostname,'listingid':$category,'source':$network},
		type:"POST",
		dataType:"json",
		success: function(response){	
			//console.log(response);			
			var $streetview = $(document).find('#map_street_view');
			if( $streetview.length > 0 ){
				panorama = GMaps.createPanorama({
					el: '#map_street_view',
					lat : response.latitude,
					lng : response.longitude,
				});
			} //End of Company Street View
			
		}
	})
  })

  $(document).on('click','#player-show',function(){
	var $hostname = window.location.hostname;
	var $category = getsearchcategory();
	
	$.ajax({
		url:"/assets/common/ajax/getlocation",
		data: {'hostname':$hostname,'listingid':$category,'source':$network},
		type:"POST",
		dataType:"json",
		success: function(response){	
			$('#player-container').html(response.player);
			//$(document).find('.map-direction-container-inside').html(response.player);//.css('max-width','768px').css('margin','0 auto').parent('.map-wrapper').css('text-align','center');
		}
	})
  })

  $(document).on('click','.nav-tabs',function(event){
	  if(event.target.text == 'Contact'){
	  		var $hostname = window.location.hostname;
			var $category = getsearchcategory();
			
			$.ajax({
				url:"/assets/common/ajax/getlocation",
				data: {'hostname':$hostname,'listingid':$category,'source':$network},
				type:"POST",
				dataType:"json",
				success: function(response){	
					//console.log(response);	
					if(loaded == false){		
						var $contactmap = $(document).find('#contact_map_canvas_one');
						if( $contactmap.length > 0 ){
							$contactmap.removeData();
							$contactmap.each(function() {
								$(this).goMap({
									maptype: 'ROADMAP',
									zoom: 13,
									scrollwheel: false,
									markers: JSON.parse(response.marker)
								});
							})
						}
						loaded = true;
					}
				}
			})
		}
  })
  
  $.ajax({
		url:"/assets/common/ajax/getlocation",
		data: {'hostname':$hostname,'listingid':$category,'source':$network},
		type:"POST",
		dataType:"json",
		beforeSend: function(){
			//$(document).find('.company-heading-view .container').css('opacity','0');
		},
		success: function(response){
			if( $(response.player).find('iframe').length > 0 ){ 
				$('#player-container').html(response.player);
				$(document).find('.video-player-container').html(response.player);//.css('max-width','768px').css('margin','0 auto').parent('.map-wrapper').css('text-align','center');
				$(document).find('.general-view-btn').show();
			}else{
				$(document).find('.video-player-container').hide();
				$(document).find('.general-view-btn').hide();
				$(document).find('.map-view-btn').css('margin-left','0px').addClass('active');
				$(document).find('.company-slider-content').css('left','-100%');
			}
			$(document).find('.company-heading-view > .container').show();
		}
	})
  
  $('.bxslider').bxSlider({startSlide: 0 , controls: false , responsive: true});
  
  if( $(document).find('#all-list-categories').length > 0 ){
	  console.log('FOUND CATEGORIES CONTAINER');  
  }
  
  $('#all-list-categories').find('.show').on('click', function (event) {
      event.preventDefault();

      var $this = $(this),
        $li = $this.parent('li'),
        $div = $this.siblings('div'),
        $siblings = $li.siblings('li').children('div');

      if (!$li.hasClass('active')) {
        $siblings.slideUp(250, function () {
          $(this).parent('li').removeClass('active');
        });

        $div.slideDown(250, function () {
          $li.addClass('active');
        });
      } else {
        $div.slideUp(250, function () {
          $li.removeClass('active');
        });
      }


  });


})(jQuery);