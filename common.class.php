<?php
/**
* @version   WWBN Websites Class 11/11/2015
* @author    Bennex Louis Edquiba
* @copyright Copyright (C) 2007 - 2015 WWBN
*/ 

class Websites{
	
	var $domainsite = array();
	var $searchads = array();
	var $shortener = array();
	var $activeacctid = "";
	var $uidfk = "";
	var $activepath = "";
	var $companyaccountid = "";
	var $regfirst = "";
	var $regsecond = "";
	var $reganswer = "";
	var $useregfirst = "";
	var $useregsecond = "";
	var $usereganswer = "";

	
	/**
	 * Constructor - Start a session needed for other functions
	 * @since 3.0 2015
	 **/
	public function __construct(){
		if(!isset($_SESSION)){
			session_start();
		}
		
		$this->activeacctid = $_SESSION['activeacctid'];
		$this->uidfk = $_SESSION['uidfk'];
		$this->activepath = $_SESSION['topdirectory'];
		$this->companyaccountid = (isset($_SESSION['UserCompanyAcctId'])) ? $_SESSION['UserCompanyAcctId'] : "";
		
		$this->getDatabaseConnect();
	}
	
	/**
	 * getDatabaseConnect - Start a session needed for other functions
	 * @since 3.0 2015
	 **/
	function getDatabaseConnect(){
		// connection with parameters
		$searchads_params = array(
			'dbname'=>'searchads-db',
			'dbuser'=>DBUSERCONFIG,
			'dbpass'=>DBPASSCONFIG,
			'dbhost'=>DBHOSTCONFIG, // optional
			//'dbencoding'=>'dbencoding', // optional
		);
		
		// connection with parameters
		$domainsite_params = array(
			'dbname'=>'domain_site',
			'dbuser'=>DBUSERCONFIG,
			'dbpass'=>DBPASSCONFIG,
			'dbhost'=>DBHOSTCONFIG, // optional
			//'dbencoding'=>'dbencoding', // optional
		);
		
		// connection with parameters
		$shortener_params = array(
			'dbname'=>'url_shortner2',
			'dbuser'=>DBUSERCONFIG,
			'dbpass'=>DBPASSCONFIG,
			'dbhost'=>DBHOSTCONFIG, // optional
			//'dbencoding'=>'dbencoding', // optional
		);
		
		$this->domainsite = $domainsite_params;
		$this->searchads = $searchads_params;
		$this->shortener = $shortener_params;
	}
	
	/**
	* CLEAN
	* Cleans out a text and strip potentially dangerous tags.
	* @since 3.0
	**/
	public function clean($text, $full= FALSE){
		$text=preg_replace('/<script[^>]*>([\s\S]*?)<\/script[^>]*>/i', '', $text);			
		if($full){
			$search = array('@<script[^>]*?>.*?</script>@si',
						 '@<[\/\!]*?[^<>]*?>@si',
						 '@<style[^>]*?>.*?</style>@siU',
						 '@<![\s\S]*?--[ \t\n\r]*>@'
			); 
			$text = preg_replace($search, '', $text);			
		}else{
			$text=strip_tags($text,'<b><i><s><u><a><pre><code><p>');
			$text=str_replace('href=','rel="nofollow" href=', $text);
		}
		return $text;
	}
	
	/**
	* urlencodeall
	* Create and formulate a pagination for search result.
	* @since 3.0
	**/
	function urlencodeall($x) {
		$out = '';
		for ($i = 0; isset($x[$i]); $i++) {
			$c = $x[$i];
			if (!ctype_alnum($c)) $c = '%' . sprintf('%02X', ord($c));
			$out .= $c;
		}
		return $out;
	}
	
	/**
	* PAGINATION
	* Create and formulate a pagination for search result.
	* @since 3.0
	**/
	function pagination($total, $current, $list = TRUE, $location='USA', $withul = FALSE, $limit='20', $class='pagination', $activeclass='active'){
		$page_count = ceil($total/$limit);
		$current_range = array(($current-5 < 1 ? 1 : $current-3), ($current+5 > $page_count ? $page_count : $current+3));
		
		if($list == TRUE){
			$first_page = $current > 3 ? '<li><a class="pagebtn" data-location="'.$location.'" href="1">1</a></li>'.($current < 5 ? ' ' : ' ') : null;
			$last_page = $current < $page_count-2 ? ($current > $page_count-4 ? ' ' : ' ').'<li><a class="pagebtn" data-location="'.$location.'" href="'.$page_count.'">'.$page_count.'</a></li>' : null;
		 
			if($current>1){ $prev = $current -1; }else{ $prev = 1; }
			if($current<=$page_count){ $next = $current + 1; }else{ $next = $page_count; }
		 
			$previous_page = $current > 1 ? '<li><a style="cursor:pointer" data-location="'.$location.'" data-page="'.$prev.'" class="prev pagebtn"> &lsaquo; </a></li> ' : null;
			$next_page = $current < $page_count ? ' <li><a style="cursor:pointer" data-location="'.$location.'" data-page="'.$next.'" class="next pagebtn"> &rsaquo; </a></li> ' : null;
	
			for ($x=$current_range[0];$x <= $current_range[1]; ++$x)    
				$pages[] = ($x == $current ? '<li class="'.$activeclass.'" data-total="'.$total.'"><a class="active pagebtn" data-location="'.$location.'" style="cursor:pointer" data-page='.$x.'>'.$x.'</a></li>' : '<li><a class="pagebtn" data-location="'.$location.'" style="cursor:pointer" data-page='.$x.'>'.$x.'</a></li>');
			if ($page_count > 1){
				if($withul != FALSE){
					return '<ul class="'.$class.'">'.$previous_page.implode(' ', $pages).$next_page.'</ul>';
				}else{
					return $previous_page.implode(' ', $pages).$next_page;
				}
			}
		}else{
			$first_page = $current > 3 ? '<a class="pagebtn" data-location="'.$location.'" href="1">1</a>'.($current < 5 ? ' ' : ' ') : null;
			$last_page = $current < $page_count-2 ? ($current > $page_count-4 ? ' ' : ' ').'<a class="pagebtn" data-location="'.$location.'" href="'.$page_count.'">'.$page_count.'</a>' : null;
			 
			if($current>1){ $prev = $current -1; }else{ $prev = 1; }
			if($current<=$page_count){ $next = $current + 1; }else{ $next = $page_count; }
			 
			$previous_page = $current > 1 ? '<a style="cursor:pointer" data-location="'.$location.'" data-page="'.$prev.'" class="prev pagebtn"> &lsaquo; </a> ' : null;
			$next_page = $current < $page_count ? ' <a style="cursor:pointer" data-location="'.$location.'" data-page="'.$next.'" class="next pagebtn"> &rsaquo; </a> ' : null;
		
			for ($x=$current_range[0];$x <= $current_range[1]; ++$x)    
				$pages[] = ($x == $current ? '<a class="'.$activeclass.' pagebtn" data-location="'.$location.'" style="cursor:pointer" data-page='.$x.'>'.$x.'</a>' : '<a class="pagebtn" data-location="'.$location.'" style="cursor:pointer" data-page='.$x.'>'.$x.'</a>');
			if ($page_count > 1)
				return $previous_page.implode(' ', $pages).$next_page;
		}
	}
	
	public function loginuser($username,$password){
		$uname = $username;
		$pass = md5( $password );
		
		$users = Sdba::table('users');
		$users->where('username',$uname)->and_where('password',$pass);
		$userinfo = $users->get_one();
		
		$this->uidfk = $_SESSION['uidfk'] = $userinfo['uid'];
		
		if( !empty($userinfo) ){
			$logininfo = array('status'=>'ok','userid'=>$userinfo['uid']);
		}else{
			$logininfo = array('status'=>'error','userid'=>'');	
		}
		
		return json_encode($logininfo);
	}
	
	public function logoutuser(){
		unset($_SESSION['uidfk']);
		$this->uidfk == "";
	}
	
	/**
	* YPAPICALL
	* Call the Yellow Page API and get the result
	* @since 3.0
	**/
	public function ypsearchlisting($location, $category, $radius = 1, $page = 1){
		
		//API Call
		#define('APIURL','http://api2.yp.com/listings/v1/search?searchloc='.$location.'&term='.$category.'&format=json&sort=distance&radius='.$radius.'&listingcount=20&pagenum='.$page.'&key=2mw1yc8fzs');
		#define('APIURL','http://APIDOMAINCONFIG/?fmt=json&Terms='.$category.'&hpp=20&page='.$page.'&strict=1&cdata=1&affiliate=AVIDEO.COM');
		define('APIURL','http://APIDOMAINCONFIG/index/fmt::json/Terms::'.$category.'/Location::'.$location.'/Radius::'.$radius.'/hpp::15/page::'.$page.'/strict::1/cdata::1/affiliate::AVIDEO.COM/');

		//CURL Call
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, APIURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$data = curl_exec($ch);
		curl_close($ch);
		
		#file_put_contents('/tmp/curldata.txt', print_r($data, true));
		
		$datadecoded = json_decode($data);

		return $datadecoded;
	}
	
	/**
	* YPAPICALL
	* Call the Yellow Page API and get the result
	* @since 3.0
	**/
	public function getdirections($origin, $destination){
		//API Call
		define('GOOGLE_DIRECTIONS_API','https://maps.googleapis.com/maps/api/directions/json?origin='.$origin.'&destination='.$destination.'&sensor=false&key=AIzaSyDmCCNMCEktK5GNgeIWRNKbjM2egFFetMQ');

		//CURL Call
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, GOOGLE_DIRECTIONS_API);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$data = curl_exec($ch);
		curl_close($ch);

		$directions = json_decode($data);

		return $directions;
	}
	
	/**
	* ypextractlistings
	* Call the Yellow Page API and get the listings from the API result
	* @since 3.0
	**/
	public function ypextractlistings($domain, $location, $category, $radius = 5, $page = 1){
		$domainencode = $this->getdomain($domain);
		$sitelink = $this->getsitesconnect($domainencode);
		$domaininfo = json_decode($sitelink); //{"domainsiteid":"1994","siteidkey":"99"}
		$domainsiteid = $domaininfo->domainsiteid;
		$siteidkey = $domaininfo->siteidkey;
		$domaindetails = $this->getdetails($domainsiteid);
		
		$result = $this->ypsearchlisting($location, $category, $radius, $page);
		
		//$listings =  $result->searchResult->searchListings->searchListing;
		//$properties = $result->searchResult->metaProperties;
		
		$listings =  $result->listing;
		#$listings =  $result->results->listing;
		$properties = $result->searchinfo;
		
		$listingtotal = $this->getlistingtotal($properties);
		$listingresult = $this->createlistingshtml($listings,$domaindetails);
		
		$paginate = $this->getpaginationdetails($domaindetails);
		if(empty($paginate)){
			$pagination = $this->pagination($listingtotal,$page,TRUE,$location);
			$paginatecontainer = 'pagination';
		}else{
			$pagination = $this->pagination($listingtotal,$page,$paginate->isList,$location,$paginate->withUl,20,$paginate->className,$paginate->currentClass);
			$paginatecontainer = $paginate->containerClass;
		}
		$mapmarkerhtml = (!empty(trim($domaindetails['mapmarker'])))?trim($domaindetails['mapmarker']):NULL;
		$markers = $this->createmarkers($listings,$mapmarkerhtml);
		$markersid = $this->getmarkersid($listings);
		$addbutton = $this->makebuttonaddlisting($category);
		
		$jsonresult = json_encode($result);
		
		$jsoninfo = array(
						'paginate'=>$paginate,
						'properties'=>$properties,
						'listings'=>$listingresult,
						'markers'=>$markers,
						'markersid'=>$markersid,
						'pagination'=>$pagination,
						'paginationContainer'=>$paginatecontainer,
						'location'=>$location,
						'category'=>$category,
						'result'=>$jsonresult,
						'addbutton'=>$addbutton,
						'page'=>$page
						);
		$jsoninfo = json_encode($jsoninfo);
		return $jsoninfo;
	}
	
	/**
	* createmarkersonclick
	* Call the Yellow Page API and get the listings from the API result
	* @since 3.0
	**/
	public function createmarkersonclick($domain, $location, $category, $radius = 5, $page = 1){
		$domainencode = $this->getdomain($domain);
		$sitelink = $this->getsitesconnect($domainencode);
		$domaininfo = json_decode($sitelink); //{"domainsiteid":"1994","siteidkey":"99"}
		$domainsiteid = $domaininfo->domainsiteid;
		$siteidkey = $domaininfo->siteidkey;
		$domaindetails = $this->getdetails($domainsiteid);
		
		$result = $this->ypsearchlisting($location, $category, $radius, $page);
		
		$listings =  $result->listing;
		$properties = $result->searchinfo;
		
		$listingtotal = $this->getlistingtotal($properties);
		$listingresult = $this->createlistingshtml($listings,$domaindetails);
		
		$mapmarkerhtml = (!empty(trim($domaindetails['mapmarker'])))?trim($domaindetails['mapmarker']):NULL;
		
		$markers = $this->createmarkers($listings,$mapmarkerhtml,$category);
		$markersid = $this->getmarkersid($listings);

		$jsoninfo = array(
						'markers'=>$markers,
						'markersid'=>$markersid,
						);
		$jsoninfo = json_encode($jsoninfo);
		return $jsoninfo;
	}
	
	/**
	* getpaginationdetails
	* Extract data from the api result then convert to html
	* @since 3.0
	**/
	public function getpaginationdetails($domaindetails){
		$domaindetailsjson = json_decode($domaindetails['pagination']);
		return $domaindetailsjson;
	}
	
	/**
	* createlistingshtml
	* Extract data from the api result then convert to html
	* @since 3.0
	**/
	public function createlistingshtml($listings,$domaindetails){
		
		$i = 0; $result = array();
		foreach($listings as $dataCol){
			
			parse_str(parse_url(APIDOMAINCONFIG.$dataCol->redirect, PHP_URL_QUERY), $array);
			
			
			if(isset($array['ID'])){
				$lstngID = $array['ID'];
			}
			
			$result[$i]['name'] 		= (!empty($dataCol->title))?$dataCol->title:'Title Not Available';
			$result[$i]['lat'] 			= $dataCol->latitude;
			$result[$i]['long'] 		= $dataCol->longitude;
			$result[$i]['primary'] 		= $dataCol->primarycategory;
			$result[$i]['description'] 	= $dataCol->description;
			$result[$i]['categories'] 	= $dataCol->categories;
			$result[$i]['services'] 	= strip_tags($dataCol->services);
			$result[$i]['slogan'] 		= $dataCol->slogan;
			$result[$i]['street'] 		= $dataCol->street;
			$result[$i]['city'] 		= $dataCol->city;
			$result[$i]['state'] 		= $dataCol->state;
			$result[$i]['zip'] 			= $dataCol->zip;
			$result[$i]['rating'] 		= trim($dataCol->rating);
			$result[$i]['listingid'] 	= ( !empty( $dataCol->listingid ) ) ? $dataCol->listingid : $lstngID;
			$result[$i]['network'] 		= $dataCol->network;
			$result[$i]['redirect'] 	= $dataCol->redirect;
			$result[$i]['adimage'] 		= $dataCol->adimage;
			
			$i++;
		}
		
		$listingresult = "";
		foreach($result as $resultcol => $resultval){
			extract($resultval);
			
			$url = "https://maps.googleapis.com/maps/api/staticmap?center=".$lat.",".$long."&zoom=15&size=140x140&maptype=roadmap&markers=color:green%7C".$lat.",".$long."&key=AIzaSyBRempgOp7bkStSsFGif6TSCEigM7DdzjQ";
			$listingresulthtml = $domaindetails['listingresult'];
			$imagelogo = (!empty(trim($adimage))) ? $adimage : '/assets/common/images/maps/maps-icon-default-3.png';
			$stars = $this->createratingstar($rating);
			
			$replace = str_replace('[COMPANYLOGO]','<img src="'.$imagelogo.'" alt="COMPANY LOGO">',$listingresulthtml);
			$replace = str_replace('[COMPANYLISTINGID]',$listingid.'/'.$network,$replace);
			$replace = str_replace('[COMPANYNAME]',$name,$replace);
			$replace = str_replace('[COMPANYCATEGORIES]', implode( ' &bull; ', explode('|',$services) ) ,$replace);
			$replace = str_replace('[COMPANYDESCRIPTION]', !empty($categories) ? html_entity_decode(implode(' &bull; ',explode( '|', rtrim( preg_replace('/[,]/', '', $categories),'|')))) : $description, $replace );
			$replace = str_replace('[COMPANYRATINGSTARS]', $stars, $replace);
			
			$listingresult .= $replace;
		}
		
		return $listingresult;
	}
	
	public function createmarkers($listings,$html=NULL,$category=''){
		
		switch(trim($category)){
			case "architects": 
				$icon = "/assets/90/images/category_icons/architects_marker.png"; 
			break;
			case "brickwork": 
				$icon = "/assets/90/images/category_icons/brick_work_marker.png"; 
			break;
			case "concrete": 
				$icon = "/assets/90/images/category_icons/concrete_marker.png"; 
			break;
			case "decking": 
				$icon = "/assets/90/images/category_icons/decking_marker.png"; 
			break;
			case "demolition": 
				$icon = "/assets/90/images/category_icons/demolition_marker.png"; 
			break;
			case "electrical": 
				$icon = "/assets/90/images/category_icons/electrical_marker.png"; 
			break;
			case "elevator": 
				$icon = "/assets/90/images/category_icons/elevator_marker.png"; 
			break;
			case "financial": 
				$icon = "/assets/90/images/category_icons/financial_marker.png"; 
			break;
			case "flooding": 
				$icon = "/assets/90/images/category_icons/flooding_marker.png"; 
			break;
			case "gate": 
				$icon = "/assets/90/images/category_icons/gate_marker.png"; 
			break;
			case "golf": 
				$icon = "/assets/90/images/category_icons/golf_marker.png"; 
			break;
			case "ground": 
				$icon = "/assets/90/images/category_icons/ground_turf_marker.png"; 
			break;
			case "handyman": 
				$icon = "/assets/90/images/category_icons/handyman_marker.png"; 
			break;
			case "heatingcooling": 
				$icon = "/assets/90/images/category_icons/heating_cooling_marker.png"; 
			break;
			case "insurance": 
				$icon = "/assets/90/images/category_icons/insurance_marker.png"; 
			break;
			case "it": 
				$icon = "/assets/90/images/category_icons/it_marker.png"; 
			break;
			case "janitorial": 
				$icon = "/assets/90/images/category_icons/janitorial_marker.png"; 
			break; 
			case "lakestream": 
				$icon = "/assets/90/images/category_icons/lake_stream_marker.png"; 
			break; 
			case "landscaping": 
				$icon = "/assets/90/images/category_icons/landscaping_marker.png"; 
			break;
			case "legal": 
				$icon = "/assets/90/images/category_icons/legal_marker.png"; 
			break;
			case "mailbox": 
				$icon = "/assets/90/images/category_icons/mailbox_marker.png"; 
			break;
			case "painting": 
				$icon = "/assets/90/images/category_icons/painting_marker.png"; 
			break;
			case "pest": 
				$icon = "/assets/90/images/category_icons/pest_control_marker.png"; 
			break;
			case "playgrounds": 
				$icon = "/assets/90/images/category_icons/playgrounds_marker.png"; 
			break;
			case "plumbing": 
				$icon = "/assets/90/images/category_icons/plumbing_marker.png"; 
			break;
			case "pool": 
				$icon = "/assets/90/images/category_icons/pool_marker.png"; 
			break;
			case "recycling": 
				$icon = "/assets/90/images/category_icons/recycling_marker.png"; 
			break;
			case "reserve": 
				$icon = "/assets/90/images/category_icons/reserve_studies_marker.png"; 
			break;
			case "roadways": 
				$icon = "/assets/90/images/category_icons/roadways_marker.png"; 
			break;
			case "roofing":
				$icon = "/assets/90/images/category_icons/roofing_marker.png";
			break;
			case "security": 
				$icon = "/assets/90/images/category_icons/security_marker.png"; 
			break;
			case "surveyor": 
				$icon = "/assets/90/images/category_icons/surveyor_marker.png"; 
			break;
			case "tennis": 
				$icon = "/assets/90/images/category_icons/tennis_marker.png"; 
			break;
			default:
				$icon = '/assets/common/images/maps/company-marker-small.png';
			break;
		}
		
		$x = 0;
		foreach($listings as $mapmarker){
			$logo = (!empty(trim($mapmarker->adImage))) ? $mapmarker->adImage : '/assets/common/images/maps/maps-icon-default-2.png';
			$marker[$x]['latitude'] 	= $mapmarker->latitude;
			$marker[$x]['longitude'] 	= $mapmarker->longitude;
			$marker[$x]['icon'] 		= $icon;//'/assets/99/img/map-marker.png';
			$street 					= strip_tags($mapmarker->street);
			$marker[$x]['id']			= 'marker-'.$x;
			$marker[$x]['group'] 		= $icon;
			$listingid					= (!empty(trim($mapmarker->listingid)))? $mapmarker->listingid.'/'.$mapmarker->network : $mapmarker->id.'/'.$mapmarker->network;
			
			if($html == NULL){
			$marker[$x]['html'] 		= '<div class="marker-holder">
											<div class="marker-company-thumbnail"><img src="'.$logo.'" onerror="this.src="/assets/common/images/maps/maps-icon-default-2.png"" alt="">
												<ul class="marker-action custom-list">
												</ul>
											</div>
											<div class="map-item-info">
												<h5 class="title"><a href="/Details/'.$listingid.'">'.$mapmarker->title.'</a></h5>
												<ul class="rating custom-list">
													<li><i class="fa fa-star-o"></i></li>
													<li><i class="fa fa-star-o"></i></li>
													<li><i class="fa fa-star-o"></i></li>
													<li><i class="fa fa-star-o"></i></li>
													<li><i class="fa fa-star-o"></i></li>
												</ul>
												<div class="describe">
													<p class="contact-info address">'.$mapmarker->street.'<br/>'.$mapmarker->city.', '.$mapmarker->state.'</p>
													<p class="contact-info telephone">'.$mapmarker->phone.'</p>
													<p class="contact-info"><a href="/Details/'.$listingid.'"> View Company Details</i></p>
												</div>
											</div>
										</div>';
			}else{
				$replacehtml = str_replace('[LISTINGID]', $listingid, $html);
				$replacehtml = str_replace('[LISTINGNAME]', $mapmarker->title, $replacehtml);
				$replacehtml = str_replace('[LISTINGSTREET]', $mapmarker->street, $replacehtml);
				$replacehtml = str_replace('[LISTINGCITY]', $mapmarker->city, $replacehtml);
				$replacehtml = str_replace('[LISTINGSTATE]', $mapmarker->state, $replacehtml);
				$replacehtml = str_replace('[LISTINGPHONE]', $mapmarker->phone, $replacehtml);
				$marker[$x]['html'] = $replacehtml;
			}
			$x++;
		}
		return json_encode($marker);
	}
	
	public function getmarkersid($listings){
		$x = 0;
		foreach($listings as $mapmarker){
			$format[$x] =  'marker-'.$x;
			$x++;
		}
		return json_encode($format);
	}
	
	public function createlocationdata($data){
		$info = extract($this->listingdetailsinfo($data));
		$marker = $this->createcompanymarker($data);
		
		if(!empty($field1)){
			$alias = explode("/",parse_url($field1, PHP_URL_PATH));
			$endofurl = $alias[count($alias) -1];
			$urlshort = Sdba::table('av_url',$this->shortener);
			$urlshort->where('alias',$endofurl)->or_where('custom',$endofurl);
			$urlinfo = $urlshort->get_one();
			
			if( trim($urlinfo['url']) == "" || empty($urlinfo['url']) ){
				$player = '<p><i class="fa fa-exclamation-triangle"></i>Ooops! There were no video found for this listing.</p>'; 
			}else{
				$player = '<div class="video-wrapper-main"><iframe width="560" height="349" src="'.$urlinfo['url'].'" frameborder="0" allowfullscreen></iframe></div>';	
			}
		}else if(!empty($videourl)){
			$player = '<div class="video-wrapper-main"><iframe width="560" height="349" src="'.$videourl.'" frameborder="0" allowfullscreen></iframe></div>';	
		}else{
			$player = '<p><i class="fa fa-exclamation-triangle"></i>Ooops! There were no video found for this listing.</p>';
		}
		
		return json_encode( array('marker' => $marker, 'latitude' => $latitude, 'longitude' => $longitude, 'player'=>$player) );
	}
	
	public function createlistingdetails($data,$html,$ratehtml,$network){
		if($network == "YP"){
			return $this->createhtmlnonavideo($data,$html,$ratehtml);
		}else{
			return $this->createhtmlavideo($data,$html,$ratehtml);
		}
	}
	
	public function createhtmlnonavideo($data,$html,$ratehtml){
		$info = extract($this->listingdetailsinfo($data));
		
		//Create a marker for this company
		$marker = $this->createcompanymarker($data);
		
		//Create the company information html
		$categorieslist = ""; $categoriesinline = "";
		
		if($categories != '0'){
			foreach(array_filter(explode(",",$categories)) as $catlist){
				$categorieslist .= '<li class="tag-grey"><a href="javascript:void(0)"> '.htmlspecialchars_decode($catlist).'</a></li>';
				$categoriesinline .=  $categoryname.' ';
			}
		}else{
			$categorieslist .= '<li class="tag-grey"><a href="javascript:void(0)">No Services Found</a></li>';
		}
		
		//COMPANY INFORMATION HANDLING
		$changeinfo = str_replace('[COMPANYLOGO]',(!empty($adimage)) ? $adimage : '/assets/common/images/maps/maps-icon-default-4.png',$html);
		$changeinfo = str_replace('[COMPANYNAME]',$title,$changeinfo);
		$changeinfo = str_replace('[KEYWORDS]',$services,$changeinfo);
		$changeinfo = str_replace('[TITLETAG]',ucfirst($slogan),$changeinfo);
		$changeinfo = str_replace('[COMPANYDESCRIPTION]',(!empty($description1)) ? $description1 : 'No description found for this listing',$changeinfo);
		
		if(!empty($videourl)){
			$changeinfo = str_replace('[HIDEPLAYER]','',$changeinfo);
			$changeinfo = str_replace('[SHOWCOMPLOGO]','display:none',$changeinfo);
		}else{
			$changeinfo = str_replace('[HIDEPLAYER]','display:none',$changeinfo);
			$changeinfo = str_replace('[SHOWCOMPLOGO]','',$changeinfo);
		}
		$changeinfo = str_replace('[PLAYER]',$videourl,$changeinfo); 
		
		if(!empty($urlinfo['url']) && !empty($latitude)){
		$changeinfo = str_replace('[STREETPLAYERBUTTON]','<div class="row">
			<div class="col-sm-12"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div>
			</div>',$changeinfo); #<div class="col-sm-6"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-play"></i> Play Video</a></div>
		}else if(empty($urlinfo['url']) && !empty($latitude)){
			$changeinfo = str_replace('[STREETPLAYERBUTTON]','<div class="row"><div class="col-sm-12"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div></div>',$changeinfo);
		}else if(!empty($urlinfo['url']) && empty($latitude)){
			$changeinfo = str_replace('[STREETPLAYERBUTTON]','<div class="row"><div class="col-sm-12"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-road"></i> Play Video</a></div></div>',$changeinfo);
		}else{
			$changeinfo = str_replace('[STREETPLAYERBUTTON]','',$changeinfo);	
		}
		
		/*
		if(!empty($urlinfo['url'])){
		$changeinfo = str_replace('[PLAYERBUTTON]','<div class="row"><div class="col-sm-12"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-road"></i> Play Video</a></div></div>',$changeinfo);
		}else{
		$changeinfo = str_replace('[PLAYERBUTTON]','',$changeinfo);
		}
		*/
		$changeinfo = str_replace('[PLAYERBUTTON]','',$changeinfo);
		
		if(!empty($longitude) && !empty($latitude)){
		$changeinfo = str_replace('[MAPVIEWHIDE]','',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWHIDE]','',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWBUTTON]','<div class="row"><div class="col-sm-12"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div></div>',$changeinfo);
		}else{
		$changeinfo = str_replace('[MAPVIEWHIDE]','display:none',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWHIDE]','display:none',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWBUTTON]','',$changeinfo);	
		}
		
		//SOCIAL MEDIA HANDLING
		$soclist = explode(",",$socialnetworks);
		if(!empty($soclist[0])) $changeinfo = str_replace('[HIDEFACEBOOK]','',$changeinfo);
		else $changeinfo = str_replace('[HIDEFACEBOOK]','display:none',$changeinfo);
		if(!empty($soclist[1])) $changeinfo = str_replace('[HIDEGOOGLE]','',$changeinfo);
		else $changeinfo = str_replace('[HIDEGOOGLE]','display:none',$changeinfo);
		if(!empty($soclist[2])) $changeinfo = str_replace('[HIDETWITTER]','',$changeinfo);
		else $changeinfo = str_replace('[HIDETWITTER]','display:none',$changeinfo);
		
		//SOCIAL MEDIA HANDLING
		if($socialnetworks != "" || !empty($socialnetworks)){
		$soclist = explode(",",$socialnetworks);
		$changeinfo = str_replace('[FACEBOOKLINK]',(!empty($soclist[0]))?$soclist[0]:'javascript:void(0)',$changeinfo);
		$changeinfo = str_replace('[GOOGLELINK]',(!empty($soclist[1]))?$soclist[1]:'javascript:void(0)',$changeinfo);
		$changeinfo = str_replace('[TWITTERLINK]',(!empty($soclist[2]))?$soclist[2]:'javascript:void(0)',$changeinfo);
		}else{
		$changeinfo = str_replace('[FACEBOOKLINK]','javascript:void(0)',$changeinfo);
		$changeinfo = str_replace('[GOOGLELINK]','javascript:void(0)',$changeinfo);
		$changeinfo = str_replace('[TWITTERLINK]','javascript:void(0)',$changeinfo);
		}
		
		//OTHER INFORMATION
		$paymethods = (!empty($paymentmethods)) ? $paymentmethods : '';
		$changeinfo = str_replace('[PAYMENTMETHODS]', $paymethods,$changeinfo);
		$language = (!empty($languagesspoken)) ? $languagesspoken : 'No Information';
		$changeinfo = str_replace('[LANGUAGESPOKEN]', $language,$changeinfo);
		$businessyear = (!empty($inbusinesssince)) ? $inbusinesssince: 'No Information';
		$changeinfo = str_replace('[COMPANYFOUNDED]', $businessyear,$changeinfo);

		//CATEGORIOES AND SERVICE HANDLING
		$changeinfo = str_replace('[COMPANYMAINCATEGORY]',$categorieslist,$changeinfo);
		$changeinfo = str_replace('[COMPANYCATEGORIES]',ucfirst($titletag),$changeinfo);
		$changeinfo = str_replace('[COMPANYSERVICES]',$categorieslist,$changeinfo);
		$changeinfo = str_replace('[OWNCOMPANYBUTTON]','<a class="remodal-btn default full-width" data-remodal-target="owncompany">Own This Company</a>',$changeinfo);
		/*
		$changeinfo = str_replace('[STREETPLAYERBUTTON]','<div class="row">
			<div class="col-sm-6"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div>
			<div class="col-sm-6"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-play"></i> Play Video</a></div></div>',$changeinfo);
		$changeinfo = str_replace('[PLAYERBUTTON]','<div class="row"><div class="col-sm-12"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-road"></i> Play Video</a></div></div>',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWBUTTON]','<div class="row"><div class="col-sm-12"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div></div>',$changeinfo);
		*/
		$changeinfo = str_replace('[REVIEWBUTTON]','Ratings and Reviews are not allowed for this listing provider',$changeinfo);
		$changeinfo = str_replace('[TOTALRATING]',(!empty($rating))?$rating:'0',$changeinfo);
		$changeinfo = str_replace('[RATINGRANKING]','Cannot view ratings and reviews for this listing provider',$changeinfo);
		
		//COMPANY ADDRESS HANDLING
		$changeinfo = str_replace('[COMPANYFULLADDRESS]',$street.' '.$city.' '.$state.' '.$zip,$changeinfo);
		$changeinfo = str_replace('[STREET]',$street,$changeinfo);
		$changeinfo = str_replace('[CITY]',$city,$changeinfo);
		$changeinfo = str_replace('[STATE]',$state,$changeinfo);
		$changeinfo = str_replace('[ZIP]',$zip,$changeinfo);
		
		//CONTACT INFORMATION HANDLING
		$changeinfo = str_replace('[COMPANYPHONE]',$phone,$changeinfo);
		$changeinfo = str_replace('[COMPANYFAX]',(!empty($extrafax)) ? $extrafax : 'No Information',$changeinfo);
		$changeinfo = str_replace('[COMPANYWEBSITE]',(!empty($websiteurl)) ? $websiteurl : 'No Information',$changeinfo);
		$changeinfo = str_replace('[COMPANYEMAIL]',(!empty($extraemails)) ? $extraemails : 'No Information',$changeinfo);
		
		if(!empty(trim($detailedhours))){
		$changeinfo = str_replace('[MONDAYSCHED]',(!empty($detailedhours[0]))?$detailedhours[0]:'STORE IS CLOSED',$changeinfo);
		$changeinfo = str_replace('[TUESDAYSCHED]',(!empty($detailedhours[1]))?$detailedhours[1]:'STORE IS CLOSED',$changeinfo);
		$changeinfo = str_replace('[WEDNESDAYSCHED]',(!empty($detailedhours[2]))?$detailedhours[2]:'STORE IS CLOSED',$changeinfo);
		$changeinfo = str_replace('[THURSDAYSCHED]',(!empty($detailedhours[3]))?$detailedhours[3]:'STORE IS CLOSED',$changeinfo);
		$changeinfo = str_replace('[FRIDAYSCHED]',(!empty($detailedhours[4]))?$detailedhours[4]:'STORE IS CLOSED',$changeinfo);
		$changeinfo = str_replace('[SATURDAYSCHED]',(!empty($detailedhours[5]))?$detailedhours[5]:'STORE IS CLOSED',$changeinfo);
		$changeinfo = str_replace('[SUNDAYSCHED]',(!empty($detailedhours[6]))?$detailedhours[6]:'STORE IS CLOSED',$changeinfo);
		}else{
		$changeinfo = str_replace('[MONDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[TUESDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[WEDNESDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[THURSDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[FRIDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[SATURDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[SUNDAYSCHED]','NO INFORMATION',$changeinfo);
		}
		
		//DIRECTION INFORMATION HANDLING
		$changeinfo = str_replace('[LATITUDE]',$latitude,$changeinfo);
		$formatted = str_replace('[LONGITUDE]',$longitude,$changeinfo);
		
		$ownformatted = $this->createhtmlowncompany($data);
		$reviewsformatted = $this->createhtmlreview($data);
		$userregformatted = $this->createuserregistration();
		
		return json_encode( array('marker'=>$marker,'details'=>$formatted,'reguser'=>$userregformatted,'owncompany'=>$ownformatted,'reviews'=>$reviewsformatted,'latitude'=>$latitude,'longitude'=>$longitude) );
	}
	
	public function createhtmlavideo($data,$html,$ratehtml){
		
		$apiresult = extract($this->listingdetailsinfo($data));
		
		//Create a marker for this company
		$marker = $this->createcompanymarker($data);
		
		//Create the company information html
		$categorieslist = ""; $categoriesinline = "";
		if($category != '0'){
			foreach(explode(",",$category) as $catlist){
				$getcategory = Sdba::table('iweb_hs_categories',$this->searchads);
				$getcategory->reset();
				$getcategory->where('category_id',trim($catlist));
				$catinfo = $getcategory->get_one();
				
				$categoryname = $catinfo['category_name'];
				
				$categorieslist .= '<li class="tag-grey"><a href="javascript:void(0)">'.htmlspecialchars_decode($categoryname).'</a></li>';
				$categoriesinline .=  $categoryname.' ';
			}
		}else{
			$categorieslist .= '<li class="tag-grey"><a href="javascript:void(0)">No Services Found</a></li>';
		}
		
		//COMPANY INFORMATION HANDLING
		$changeinfo = str_replace('[COMPANYLOGO]',(!empty($site_icon)) ? $site_icon : '/assets/common/images/maps/maps-icon-default-4.png',$html);
		$changeinfo = str_replace('[ONERRORIMAGE]','/assets/common/images/maps/maps-icon-default-4.png',$changeinfo);
		$changeinfo = str_replace('[COMPANYNAME]',$name,$changeinfo);
		$changeinfo = str_replace('[KEYWORDS]',$keywords,$changeinfo);
		$changeinfo = str_replace('[TITLETAG]',ucfirst($titletag),$changeinfo);
		$changeinfo = str_replace('[COMPANYDESCRIPTION]',(!empty($description)) ? $description : 'No description found for this listing',$changeinfo);
		
		$alias = explode("/",parse_url($field1, PHP_URL_PATH));
		$endofurl = $alias[count($alias) -1];
		$urlshort = Sdba::table('av_url',$this->shortener);
		$urlshort->where('alias',$endofurl)->or_where('custom',$endofurl);
		$urlinfo = $urlshort->get_one();
		
		if(!empty($urlinfo['url'])){
			$changeinfo = str_replace('[HIDEPLAYER]','',$changeinfo);
			$changeinfo = str_replace('[SHOWCOMPLOGO]','display:none',$changeinfo);
		}else{
			$changeinfo = str_replace('[HIDEPLAYER]','display:none',$changeinfo);
			$changeinfo = str_replace('[SHOWCOMPLOGO]','',$changeinfo);
		}
		$changeinfo = str_replace('[PLAYER]',$urlinfo['url'],$changeinfo); 
		
		if(!empty($urlinfo['url']) && !empty($latitude)){
		$changeinfo = str_replace('[STREETPLAYERBUTTON]','<div class="row">
			<div class="col-sm-6"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div>
			<div class="col-sm-6"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-play"></i> Play Video</a></div></div>',$changeinfo);
		}else if(empty($urlinfo['url']) && !empty($latitude)){
			$changeinfo = str_replace('[STREETPLAYERBUTTON]','<div class="row"><div class="col-sm-12"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div></div>',$changeinfo);
		}else if(!empty($urlinfo['url']) && empty($latitude)){
			$changeinfo = str_replace('[STREETPLAYERBUTTON]','<div class="row"><div class="col-sm-12"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-road"></i> Play Video</a></div></div>',$changeinfo);
		}
		
		if(!empty($urlinfo['url'])){
		$changeinfo = str_replace('[PLAYERBUTTON]','<div class="row"><div class="col-sm-12"><a id="player-show" class="remodal-btn default full-width" data-remodal-target="player"><i class="fa fa-road"></i> Play Video</a></div></div>',$changeinfo);
		}else{
		$changeinfo = str_replace('[PLAYERBUTTON]','',$changeinfo);
		}
		
		if(!empty($longitude) && !empty($latitude)){
		$changeinfo = str_replace('[MAPVIEWHIDE]','',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWHIDE]','',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWBUTTON]','<div class="row"><div class="col-sm-12"><a id="streetview-show" class="remodal-btn default full-width" data-remodal-target="streetview"><i class="fa fa-road"></i> Streetview</a></div></div>',$changeinfo);
		}else{
		$changeinfo = str_replace('[MAPVIEWHIDE]','display:none',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWHIDE]','display:none',$changeinfo);
		$changeinfo = str_replace('[STREETVIEWBUTTON]','',$changeinfo);	
		}
		
		//SOCIAL MEDIA HANDLING
		$soclist = explode(",",$socials);
		if(!empty($soclist[0])) $changeinfo = str_replace('[HIDEFACEBOOK]','',$changeinfo);
		else $changeinfo = str_replace('[HIDEFACEBOOK]','display:none',$changeinfo);
		if(!empty($soclist[1])) $changeinfo = str_replace('[HIDEGOOGLE]','',$changeinfo);
		else $changeinfo = str_replace('[HIDEGOOGLE]','display:none',$changeinfo);
		if(!empty($soclist[2])) $changeinfo = str_replace('[HIDETWITTER]','',$changeinfo);
		else $changeinfo = str_replace('[HIDETWITTER]','display:none',$changeinfo);
		
		$changeinfo = str_replace('[FACEBOOKLINK]',(!empty($soclist[0]))?$soclist[0]:'javascript:void(0)',$changeinfo);
		$changeinfo = str_replace('[GOOGLELINK]',(!empty($soclist[1]))?$soclist[1]:'javascript:void(0)',$changeinfo);
		$changeinfo = str_replace('[TWITTERLINK]',(!empty($soclist[2]))?$soclist[2]:'javascript:void(0)',$changeinfo);
		
		//OTHER INFORMATION
		$paymethods = (!empty($paymentmethods)) ? $paymethods : '';
		$changeinfo = str_replace('[PAYMENTMETHODS]', $paymethods,$changeinfo);
		$language = (!empty($languagesspoken)) ? $languagesspoken : 'No Information';
		$changeinfo = str_replace('[LANGUAGESPOKEN]', $language,$changeinfo);
		$businessyear = (!empty($inbusinesssince)) ? $inbusinesssince: 'No Information';
		$changeinfo = str_replace('[COMPANYFOUNDED]', $businessyear,$changeinfo);

		//CATEGORIOES AND SERVICE HANDLING
		$changeinfo = str_replace('[COMPANYMAINCATEGORY]',$categorieslist,$changeinfo);
		$changeinfo = str_replace('[COMPANYCATEGORIES]',ucfirst($titletag),$changeinfo);
		$changeinfo = str_replace('[COMPANYSERVICES]',$categorieslist,$changeinfo);
		$changeinfo = str_replace('[OWNCOMPANYBUTTON]','<a class="remodal-btn default full-width" data-remodal-target="owncompany">Own This Company</a>',$changeinfo);
		$changeinfo = str_replace('[OWNCOMPANYBUTTON]','<a class="remodal-btn default full-width" data-remodal-target="owncompany">Own This Company</a>',$changeinfo);
		
		
		if( $this->uidfk != "" ){
			if($enable_ratings == '1'){
				$myrating = Sdba::table('iweb_hs_ratings',$this->searchads);
				$myrating->where('listingidfk',$id);				
				$totalrate = $myrating->sum('score');
				$totalcount = $myrating->total();
				$myratinginfo = $myrating->get(5);
				
				$totalrating = $totalrate/$totalcount;
				
				$changeinfo = str_replace('[TOTALRATING]',(!empty($totalrating))?$totalrating:'0',$changeinfo);
				
				foreach($myratinginfo as $ratingname => $ratingvalue){
					
					$stars = $this->createratingstar($ratingvalue['score']);
					
					if($enable_reviews == '1'){
						$reviews = Sdba::table('iweb_hs_reviews',$this->searchads);
						$reviews->where('id',$ratingvalue['listingidfk'])->and_where('uidfk',$ratingvalue['uidfk']);
						$reviewsinfo = $reviews->get_one();
						$reviewmessage = (!empty($reviewsinfo['review_text'])) ? $reviewsinfo['review_text'] : 'No review posted';
					}else{
						$reviewmessage = 'Review is not allowed for this listing';
					}
					
					$username = Sdba::table('users');
					$username->where('uid',$ratingvalue['uidfk']);
					$userinfo = $username->get_one();
					
					$changeratinginfo = str_replace('[STARS]',$stars,$ratehtml);
					$changeratinginfo = str_replace('[USERNAMERATE]',$userinfo['display_name'],$changeratinginfo);
					$reviewslist .= str_replace('[REVIEWMESSAGE]',$reviewmessage,$changeratinginfo);
					
				}
				
				$changeinfo = str_replace('[RATINGRANKING]',$reviewslist,$changeinfo);
				
				$myownrating = Sdba::table('iweb_hs_ratings',$this->searchads);
				$myownrating->reset();
				$myownrating->where('listingidfk',$id);				
				$myownratinginfo = $myownrating->get_one();
				
				if( !empty($myownratinginfo) ){
					$stars = $this->createratingstar($myownratinginfo['score']);
					
					if($enable_reviews == '1'){
						$reviews = Sdba::table('iweb_hs_reviews',$this->searchads);
						$reviews->reset();
						$reviews->where('id',$myownratinginfo['listingidfk'])->and_where('uidfk',$myownratinginfo['uidfk']);
						$reviewsinfo = $reviews->get_one();
						$reviewmessage = (!empty($reviewsinfo['review_text'])) ? $reviewsinfo['review_text'] : 'No review posted';
					}else{
						$reviewmessage = 'Review is not allowed for this listing';
					}
					
					$mylistingrating = str_replace('[STARS]',$stars,$ratehtml);
					$mylistingrating = str_replace('[USERNAMERATE]','YOUR RATING AND REVIEW FOR THIS LISTING',$mylistingrating);
					$ownreviewrating = str_replace('[REVIEWMESSAGE]',$reviewmessage,$mylistingrating);
					
					$changeinfo = str_replace('[REVIEWBUTTON]',$ownreviewrating,$changeinfo);
				}else{
					$changeinfo = str_replace('[REVIEWBUTTON]','<a class="remodal-btn secondary" data-remodal-target="review">Submit Review &amp; Rate</a>',$changeinfo);
				}
			}else{
				$changeinfo = str_replace('[RATINGRANKING]','Reviews and ratings are disabled',$changeinfo);
				$changeinfo = str_replace('[REVIEWBUTTON]','<a class="remodal-btn secondary" data-remodal-target="review">Submit Review &amp; Rate</a>',$changeinfo);
			}
			
		}else if( $this->uidfk == "" ){
			
			$reviewslist = "";
			if($enable_ratings == '1' || $enable_ratings == 1){
				
				$myrating = Sdba::table('iweb_hs_ratings',$this->searchads);
				$myrating->where('listingidfk',$id);
				$myratinginfo = $myrating->get();
				
				$totalrate = $myrating->sum('score');
				$totalcount = $myrating->total();
				
				$totalrating = $totalrate/$totalcount;
				
				foreach($myratinginfo as $ratingname => $ratingvalue){
					
					$stars = $this->createratingstar($ratingvalue['score']);
					
					if($enable_reviews == '1'){
						$reviews = Sdba::table('iweb_hs_reviews',$this->searchads);
						$reviews->where('id',$ratingvalue['listingidfk']);
						$reviewsinfo = $reviews->get_one();
						$reviewmessage = (!empty($reviewsinfo['review_text'])) ? $reviewsinfo['review_text'] : 'No review posted';
					}else{
						$reviewmessage = 'Review is not allowed for this listing';
					}
					
					$username = Sdba::table('users');
					$username->where('uid',$ratingvalue['uidfk']);
					$userinfo = $username->get_one();
					
					$changeratinginfo = str_replace('[STARS]',$stars,$ratehtml);
					$changeratinginfo = str_replace('[USERNAMERATE]',$userinfo['display_name'],$changeratinginfo);
					$reviewslist .= str_replace('[REVIEWMESSAGE]',$reviewmessage,$changeratinginfo);
					
				}
				
				$changeinfo = str_replace('[TOTALRATING]',$totalrating,$changeinfo);
				$changeinfo = str_replace('[RATINGRANKING]',(!empty($reviewslist)) ? $reviewslist : 'Reviews and ratings are disabled',$changeinfo);
			}else{
				$changeinfo = str_replace('[TOTALRATING]','0',$changeinfo);
				$changeinfo = str_replace('[RATINGRANKING]',(!empty($reviewslist)) ? $reviewslist : 'Reviews and ratings are disabled',$changeinfo);
			}
			
			$changeinfo = str_replace('[REVIEWBUTTON]','Login using your account to post review and ratings',$changeinfo);
		}
		
		//COMPANY ADDRESS HANDLING
		$changeinfo = str_replace('[COMPANYFULLADDRESS]',$address.', '.$city.', '.$state,$changeinfo);
		$changeinfo = str_replace('[STREET]',$address,$changeinfo);
		$changeinfo = str_replace('[CITY]',$city,$changeinfo);
		$changeinfo = str_replace('[STATE]',$state,$changeinfo);
		$changeinfo = str_replace('[ZIP]',$zip,$changeinfo);
		
		//CONTACT INFORMATION HANDLING
		$changeinfo = str_replace('[COMPANYPHONE]',$phone,$changeinfo);
		$changeinfo = str_replace('[COMPANYFAX]',(!empty($extrafax)) ? $extrafax : 'No Information',$changeinfo);
		$changeinfo = str_replace('[COMPANYWEBSITE]',(!empty($websiteurl)) ? $websiteurl : 'No Information',$changeinfo);
		$changeinfo = str_replace('[COMPANYEMAIL]',(!empty($email)) ? $email : 'No Information',$changeinfo);
		
		//HOURS HANDLING
		$hours = Sdba::table('iweb_hs_openhours',$this->searchads);
		$hours->where('ohid',$open_hours);
		$jsonhoursopen = $hours->get_one();
		$hours = json_decode($jsonhoursopen['open_hours_json']);
		
		if($enable_hours == '1'){
		$changeinfo = str_replace('[MONDAYSCHED]',($hours->monday->isClose == '0') ? $hours->monday->opening.' - '.$hours->monday->closing : 'STORE IS CLOSED' ,$changeinfo);
		$changeinfo = str_replace('[TUESDAYSCHED]',($hours->tuesday->isClose == '0') ? $hours->tuesday->opening.' - '.$hours->tuesday->closing : 'STORE IS CLOSED' ,$changeinfo);
		$changeinfo = str_replace('[WEDNESDAYSCHED]',($hours->wednesday->isClose == '0') ? $hours->wednesday->opening.' - '.$hours->wednesday->closing : 'STORE IS CLOSED' ,$changeinfo);
		$changeinfo = str_replace('[THURSDAYSCHED]',($hours->thursday->isClose == '0') ? $hours->thursday->opening.' - '.$hours->thursday->closing : 'STORE IS CLOSED' ,$changeinfo);
		$changeinfo = str_replace('[FRIDAYSCHED]',($hours->friday->isClose == '0') ? $hours->friday->opening.' - '.$hours->friday->closing : 'STORE IS CLOSED' ,$changeinfo);
		$changeinfo = str_replace('[SATURDAYSCHED]',($hours->saturday->isClose == '0') ? $hours->saturday->opening.' - '.$hours->saturday->closing : 'STORE IS CLOSED' ,$changeinfo);
		$changeinfo = str_replace('[SUNDAYSCHED]',($hours->sunday->isClose == '0') ? $hours->sunday->opening.' - '.$hours->sunday->closing : 'STORE IS CLOSED' ,$changeinfo);
		}else{
		$changeinfo = str_replace('[MONDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[TUESDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[WEDNESDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[THURSDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[FRIDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[SATURDAYSCHED]','NO INFORMATION',$changeinfo);
		$changeinfo = str_replace('[SUNDAYSCHED]','NO INFORMATION',$changeinfo);
		}
		
		//DIRECTION INFORMATION HANDLING
		$formatted = str_replace('[LATITUDE]',$latitude,$changeinfo);
		$formatted = str_replace('[LONGITUDE]',$longitude,$changeinfo);
		$formatted = str_replace('[SELECTCOUNTRYLOOKUP]',$getdropdowncountries,$changeinfo);
		
		$ownformatted = $this->createhtmlowncompany($data);
		$reviewsformatted = $this->createhtmlreview($data);
		$userregformatted = $this->createuserregistration();
		
		return json_encode( array('videourl'=>$urlinfo['url'],'marker'=>$marker,'details'=>$formatted,'reguser'=>$userregformatted,'owncompany'=>$ownformatted,'reviews'=>$reviewsformatted,'latitude'=>$latitude,'longitude'=>$longitude) );
	}
	
	public function createratingstar($value){
		switch($value){
			case'1':
				$stars = '<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>';
			break;
			case'2':
				$stars = '<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>';
			break;
			case'3':
				$stars = '<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>';
			break;
			case'4':
				$stars = '<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>';
			break;
			case'5':
				$stars = '<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>
				<li><a href="#"><i class="fa fa-star"></i></a></li>';
			break;
			default:
				$stars = '<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>
				<li><a href="#"><i class="fa fa-star-o"></i></a></li>';
			break;	
		}
		
		return $stars;	
	}
	
	public function createowningsecurityquestion(){
		$this->regfirst =  rand(1,50);
		$this->regsecond = rand(1,50);
		$this->reganswer = $this->regfirst + $this->regsecond;
	}
	
	public function createregusersecurityquestion(){
		$this->useregfirst = rand(1,50);
		$this->useregsecond = rand(1,50);
		$this->usereganswer = $this->useregfirst + $this->useregsecond;
	}
	
	public function createhtmlowncompany($data){
		$apiresult = extract($this->listingdetailsinfo($data));
		$this->createowningsecurityquestion();
		
		$html = '<form id="form_own_this_company">
					<div class="row">
						<div class="minimize-padding col-sm-12">
							<label>User Contact Information</label>
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" required="" name="contact_name" title="Complete Name *" placeholder="Complete Name *" type="text">
						</div>
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" required="" name="contact_email" title="Email Address *" placeholder="Email Address *" type="email">
						</div>
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" title="Job Title" name="contact_jobtitle" placeholder="Job Title" type="text">
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-6">
							<input class="remodal-input large" title="Phone Number" name="contact_phonenumber" placeholder="Phone Number" type="text">
						</div>
						<div class="minimize-padding col-sm-6">
							<input class="remodal-input large" title="Mobile Number" name="contact_mobilenumber" placeholder="Mobile Number" type="text">
						</div>
					</div>
					<div class="spacer-10"></div>
					<div class="row">
						<div class="minimize-padding col-sm-12">
							<label>Company Information</label>
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" value="[COMPANYNAME]" required="" name="company_name" title="Company Name" placeholder="Company Name *" type="text">
						</div>
						<div class="minimize-padding col-sm-8">
							<input class="remodal-input large" value="[STREET]" title="Room/Building/Street" name="company_addressline1" placeholder="Room/Building/Street" type="text">
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" value="[CITY]" title="City" name="company_city" placeholder="City" type="text">
						</div>
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" value="[STATE]" title="State" name="company_state" placeholder="State" type="text">
						</div>
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" value="[ZIP]" title="Zip" name="company_zip" placeholder="Zip" type="text">
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-4">
							<select class="remodal-input large" name="company_country" style="margin-top:0px !important">
								[COUNTRYOPTION]
							</select>
						</div>
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" value="[COMPANYPHONE]" title="Phone Number" name="company_phonenumber" placeholder="Phone Number" type="text">
						</div>
						<div class="minimize-padding col-sm-4">
							<input class="remodal-input large" value="[COMPANYFAX]" title="Fax Number" name="company_faxnumber" placeholder="Fax Number" type="text">
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-6">
							<input class="remodal-input large" value="[COMPANYWEBSITE]" title="Company Website Address" name="company_website" placeholder="Company Website URL" type="text">
						</div>
						<div class="minimize-padding col-sm-6">
							<input class="remodal-input large" value="[COMPANYEMAIL]" title="Company Email Address" name="company_email" placeholder="Company Email Address" type="text">
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-12">
							<label>Suggested Categories</label>
						</div>
						<div class="minimize-padding col-sm-12">
							<input class="remodal-input large" name="categories" value="[COMPANYCATEGORIES]" title="Suggested Categories" placeholder="Suggested Categories" type="text">
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-6">
							<label>Video is available for this company?
								<span style="color:#F00; font-weight:bold;">
									*
								</span>
							</label>
				
							<select class="remodal-input large" name="company_videoready" id="videoready">
								<option value="">-- Select Option --</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						<div class="minimize-padding col-sm-6">
							<label>Primary contact info for verification?
								<span style="color:#F00; font-weight:bold;">
									*
								</span>
							</label>
							<select class="remodal-input large" name="company_verused" id="verused">
								<option value="">-- Select Option --</option>
								<option value="Email">Email Verification</option>
								<option value="Regular Mail">Regular Mail Verification</option>
								<option value="Phone">Phone Verification</option>
							</select>
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row" id="yesno_container" style="display:none">
						<div class="minimize-padding col-sm-6" id="yes_container" style="display:none">
							<label>Where is it hosted?
								<span style="color:#F00; font-weight:bold;">
									*
								</span>
							</label>
							<select class="remodal-input large" name="hosting" id="hostingselect">
								<option value="">-- Select Hosting Site --</option>
								<option value="None">None</option>
								<option value="Youtube">Youtube</option>
								<option value="Vimeo">Vimeo</option>
								<option value="Dailymotion">Dailymotion</option>
								<option value="Other">Other</option>
							</select>
						</div>
						<div class="minimize-padding col-sm-6" style="display:none" id="other_hosting">
							<label>
								Other Hosting Site URL/Name
							</label>
							<input name="hostingsite" class="remodal-input large" title="Hosting Site" placeholder="Hosting Site" type="text">
						</div>
						<div class="minimize-padding col-sm-6" id="no_container" style="display:none">
							<label>Would you like to create a video?
								<span style="color:#F00; font-weight:bold;">
									*
									<span>
									</span>
								</span>
							</label>
							<select  class="remodal-input large"name="createvid" id="createvid">
								<option value="">-- Select Option --</option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row" id="wehost_container" style="display:none">
						<div class="minimize-padding col-sm-6" id="wehost">
							<label>Would you like to upload the video and host on our CDN?
								<span style="color:#F00; font-weight:bold;">
									*
									<span>
									</span>
								</span>
							</label>
							<select class="remodal-input large" name="wehostvideo" id="wehost_video">
								<option value="">-- Select Option --</option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="spacer-15"></div>
					<div class="row">
						<div class="minimize-padding col-sm-12" id="warning"> </div>
						<div class="minimize-padding col-sm-12">&nbsp;</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-12">
							<label>Security Question (Answer the question below to make us sure that you are not a bot)</label>
						</div>
					</div>
					<div class="spacer-5"></div>
					<div class="row">
						<div class="minimize-padding col-sm-5"><input type="hidden" id="reganswer" value="'.$this->reganswer.'" /><h3 class="remodal-input"> '.$this->regfirst.' + '.$this->regsecond.' = ? </h3> </div>
						<div class="minimize-padding col-sm-7"><input type="text" class="remodal-input large" id="humanreganswer" placeholder="Answer to Question" /></div>
					</div>
					<div class="spacer-20"></div>
					<div class="row">
						<div class="minimize-padding col-sm-12">
							<button class="remodal-btn large primary full-width" id="register_account" type="submit"> Submit Ownership Now</button>
						</div>
					</div>
				</form>';
		
		
		//Create the company information html
		$categoriesinline = "";
		
		if(!empty($category)){
			foreach(explode(",",$category) as $catlist){
				$getcategory = Sdba::table('iweb_hs_categories',$this->searchads);
				$getcategory->reset();
				$getcategory->where('category_id',trim($catlist));
				$catinfo = $getcategory->get_one();
				
				$categoryname = $catinfo['category_name'];
				
				$categoriesinline .=  ($categoriesinline != "") ? ','.$categoryname : $categoryname;
			}
		}
		if(!empty($categories)){
			foreach(explode(",",$categories) as $catlist){
				$categoriesinline .=  ($categoriesinline != "") ? ','.$catlist : $catlist;
			}
		}
		
		//COMPANY INFORMATION HANDLING
		$changeinfo = str_replace('[COMPANYNAME]',$title,$html);
		
		//CATEGORIOES AND SERVICE HANDLING
		$changeinfo = str_replace('[COMPANYCATEGORIES]',$categoriesinline,$changeinfo);
		$changeinfo = str_replace('[COUNTRYOPTION]',$this->createdropdowncountries(),$changeinfo);
		
		//COMPANY ADDRESS HANDLING
		$changeinfo = str_replace('[STREET]',(!empty($street))?$street:$address,$changeinfo);
		$changeinfo = str_replace('[CITY]',$city,$changeinfo);
		$changeinfo = str_replace('[STATE]',$state,$changeinfo);
		$changeinfo = str_replace('[ZIP]',$zip,$changeinfo);
		
		//CONTACT INFORMATION HANDLING
		$changeinfo = str_replace('[COMPANYPHONE]',$phone,$changeinfo);
		$changeinfo = str_replace('[COMPANYFAX]',(!empty($extrafax)) ? $extrafax : '',$changeinfo);
		$changeinfo = str_replace('[COMPANYWEBSITE]',(!empty($websiteurl)) ? $websiteurl : '',$changeinfo);
		$changeinfo = str_replace('[COMPANYEMAIL]',(!empty($email)) ? $email : '',$changeinfo);
		
		return $changeinfo;
	}
	
	public function createuserregistration(){
		$this->createregusersecurityquestion();
		$html = '<form id="directory-reguser-form">
			<div class="row">
				<div class="col-sm-4 minimize-padding">
					<input type="text" placeholder="Username" name="username" class="remodal-input" required />
				</div>
				<div class="col-sm-4 minimize-padding">
					<input type="password" placeholder="Password" name="password" id="password" class="remodal-input" required />
				</div>
				<div class="col-sm-4 minimize-padding">
					<input type="password" placeholder="Confirm Password" id="confirm" class="remodal-input" required />
				</div>
			</div>
			<div class="spacer-5"></div>
			<div class="row">
				<div class="col-sm-6 minimize-padding">
					<input type="text" placeholder="First Name" name="first_name" class="remodal-input" required />
				</div>
				<div class="col-sm-6 minimize-padding">
					<input type="text" placeholder="Last Name" name="last_name" class="remodal-input" required />
				</div>
			</div>
			<div class="spacer-5"></div>
			<div class="row">
				<div class="col-sm-6 minimize-padding">
					<input type="email" placeholder="Email Address" name="email" class="remodal-input" required />
				</div>
				<div class="col-sm-6 minimize-padding">
					<input type="text" placeholder="Primary Contact Number" name="phone" class="remodal-input" required />
				</div>
			</div>
			<div class="spacer-5"></div>
			<div class="row">
				<div class="col-sm-8 minimize-padding">
					<input type="text" placeholder="Hometown" class="remodal-input" name="hometown" />
				</div>
				<div class="col-sm-4 minimize-padding">
					<select name="gender" class="remodal-input">
						<option value=""> Select Gender </option>
						<option value="male">Male</option>
						<option value="female">Female</option>
					</select>
				</div>
			</div>
			<div class="spacer-5"></div>
			<div class="row">
				<div class="col-sm-8 minimize-padding">
					<input type="text" placeholder="Street" class="remodal-input" name="usr_address" />
				</div>
				<div class="col-sm-4 minimize-padding">
					<input type="text" placeholder="City" class="remodal-input" name="usr_city" />
				</div>
			</div>
			<div class="spacer-5"></div>
			<div class="row">
				<div class="col-sm-4 minimize-padding">
					<select name="usr_state" class="remodal-input">
						<option value="">Select State or Region</option>
						[REGIONOPTION]
					</select>
				</div>
				<div class="col-sm-4 minimize-padding">
					<select type="text" name="usr_country" class="remodal-input">
						<option value="">Select Country</option>
						[COUNTRYOPTION]
					</select>
				</div>
				<div class="col-sm-4 minimize-padding">
					<input type="text" name="usr_zip" placeholder="Zipcode" class="remodal-input" />
				</div>
			</div>
			<div class="spacer-15"></div>
			<div class="row">
				<div class="minimize-padding col-sm-12">
					Security Question (Answer the question below to make us sure that you are not a bot)
				</div>
			</div>
			<div class="spacer-5"></div>
			<div class="row" id="regusersecquestion">
				<div class="minimize-padding col-sm-5"><input type="hidden" id="usereganswer" value="'.$this->usereganswer.'" /><h3 class="remodal-input"> '.$this->useregfirst.' + '.$this->useregsecond.' = ? </h3> </div>
				<div class="minimize-padding col-sm-7"><input type="text" required class="remodal-input large" id="humanusereganswer" placeholder="Answer to Question" /></div>
			</div>
			<div class="spacer-15"></div>
			<div class="row">
				<div class="minimize-padding col-sm-12" id="user-confirmation-registration">
				</div>
			</div>
			<div class="spacer-20"></div>
			<div class="row">
				<div class="col-sm-12 minimize-padding">
					<button type="submit" class="remodal-btn secondary full-width">REGISTER NOW</button>
				</div>
			</div>
			</form>
			<!--<div class="spacer-20"></div>
			<div class="spacer-20"></div>
			<div class="row">
				<div class="col-sm-12 minimize-padding">
					<hr>
				</div>
			</div>
			<div class="spacer-20"></div>
			<div class="spacer-20"></div>
			<h1><strong>Social Media Registration</strong></h1>
			<p>CREATE AN ACCOUNT USING YOUR SOCIAL MEDIA ACCOUNT</p>
			<div class="spacer-5"></div>
			<div class="row">
				<div class="col-sm-12 minimize-padding pull-center">
					<button class="remodal-btn square soc-facebook"><i class="fa fa-facebook fa-2x"></i></button>
					<button class="remodal-btn square soc-twitter"><i class="fa fa-twitter fa-2x"></i></button>
					<button class="remodal-btn square soc-googleplus"><i class="fa fa-google-plus fa-2x"></i></button>
				</div>
			</div>
			<div class="spacer-20"></div>-->';
		
		$changeinfo = str_replace('[COUNTRYOPTION]',$this->createdropdowncountries(),$html);
		$changeinfo = str_replace('[REGIONOPTION]',$this->createdropdownregion(),$changeinfo);
		
		return $changeinfo;
	}
	
	public function createhtmlreview($data){
		$apiresult = extract($this->listingdetailsinfo($data));
		
		if(empty($id)){
			$html = '<div class="row">
						<div class="col-sm-12 minimize-padding pull-center">
							<p><i class="fa fa-exclamation-triangle"></i> There was an error trying to get the listing information. Make sure that this is a valid listing with proper information.</p>
						</div>
					</div>';
					
			return $html;
		}else{
			$html = '<form id="directory-ratereview-form">
					<input type="hidden" name="listingid" value="'.$id.'" />
					<input type="hidden" name="uidfk" value="'.$this->uidfk.'" />
					<div class="row">
						<div class="col-sm-4 minimize-padding">
							<img width="100%" src="[COMPANYLOGO]" height="auto" />
							<p class="pull-left margin-zero"><strong><small>[COMPANYNAME]</small></strong><p>
							<p class="pull-left margin-zero"><small>[COMPANYCATEGORIES]</small></p>
						</div>
						<div class="col-sm-8 minimize-padding"><textarea style="resize:none; min-height:100px" placeholder="Tell us what you think about this listing. Would you recommend it and why?" title="Your Review" name="review_text" class="remodal-input"></textarea>
							<div class="row">
								<div class="col-sm-12 minimize-padding" id="review-confirmation"></div>
							</div>
							<div class="spacer-5"></div>
							<select name="score" title="Your Rating" class="remodal-input" required><option value=""> Please select a rating </option><option value="1">Bad</option><option value="2">Not Good</option><option value="3">Good</option><option value="4">Very Good</option><option value="5">Great</option></select>
							<div class="spacer-5"></div>
							<button type="submit" class="remodal-btn large primary full-width">SUBMIT REVIEW</button>
							<div class="spacer-10"></div>
						</div>
					</div>
					</form>';
			
			//Create the company information html
			$categoriesinline = "";
			foreach(explode(",",$category) as $catlist){
				$getcategory = Sdba::table('iweb_hs_categories',$this->searchads);
				$getcategory->reset();
				$getcategory->where('category_id',trim($catlist));
				$catinfo = $getcategory->get_one();
				
				$categoryname = $catinfo['category_name'];
				$categoriesinline .=  ($categoriesinline != "") ? ','.$categoryname : $categoryname;
			}
			
			//COMPANY INFORMATION HANDLING
			$changeinfo = str_replace('[COMPANYNAME]',$name,$html);
			$changeinfo = str_replace('[COMPANYLOGO]',(!empty($site_icon)) ? $site_icon : '/assets/common/images/no_logo.png',$changeinfo);
			
			//CATEGORIOES AND SERVICE HANDLING
			$changeinfo = str_replace('[COMPANYCATEGORIES]',$categoriesinline,$changeinfo);
			
			return $changeinfo;
		}
		
	}
	
	public function listingdetailsinfo($data){
		
		$info['id']						=	$data[0]->id;
		
		$info['title']					=	$data[0]->title;
		$info['description']			=	$data[0]->description;
		$info['email']					=	$data[0]->email;
		$info['name']					=	$data[0]->name;
		$info['address']				=	$data[0]->address;
		$info['city']					=	$data[0]->city;
		$info['state']					=	$data[0]->state;
		$info['zip']					=	$data[0]->zip;
		$info['country']				=	$data[0]->country;
		$info['latitude']				=	$data[0]->latitude;
		$info['longitude']				=	$data[0]->longitude;
		$info['phone']					=	$data[0]->phone;
		
		$info['video']					=	$data[0]->video;
		$info['thumbnail']				=	$data[0]->thumbnail;
		$info['redirect']				=	$data[0]->redirect;
		$info['score']					=	$data[0]->score;
		$info['bid']					=	$data[0]->bid;
		$info['hits']					=	$data[0]->hits;
		$info['top_votes']				=	$data[0]->top_votes;
		$info['network']				=	$data[0]->network;
		$info['category']				=	$data[0]->category;
		$info['extra_link']				=	$data[0]->extra_link;
		$info['rank']					=	$data[0]->rank;
		$info['qualifier']				=	$data[0]->qualifier;
		$info['url']					=	$data[0]->url;
		$info['desc_image']				=	$data[0]->desc_image;
		$info['payment_methods']		=	$data[0]->payment_methods;
		$info['startdate']				=	$data[0]->startdate;
		$info['enddate']				=	$data[0]->enddate;
		$info['price']					=	$data[0]->price;
		$info['xfactor']				=	$data[0]->xfactor;
		$info['rate_votes']				=	$data[0]->rate_votes;
		$info['rate_score']				=	$data[0]->rate_score;
		$info['av_links']				=	$data[0]->av_links;
		$info['last_status']			=	$data[0]->last_status;
		$info['open_hours']				=	$data[0]->open_hours;
		$info['adult']					=	$data[0]->adult;
		$info['keywords']				=	$data[0]->keywords;
		$info['titletag']				=	$data[0]->titletag;
		$info['about']					=	$data[0]->about;
		$info['site_icon']				=	$data[0]->site_icon;
		$info['enable_ratings']			=	$data[0]->enable_ratings;
		$info['enable_streetview']		=	$data[0]->enable_streetview;
		$info['enable_map']				=	$data[0]->enable_map;
		$info['start_date']				=	$data[0]->start_date;
		$info['end_date']				=	$data[0]->end_date;
		$info['enable_hours']			=	$data[0]->enable_hours;
		$info['enable_comments']		=	$data[0]->enable_comments;
		$info['enable_votes']			=	$data[0]->enable_votes;
		$info['enable_reviews']			=	$data[0]->enable_reviews;
		$info['socials']				=	$data[0]->socials;
		$info['field1']					=	$data[0]->field1;
		$info['field2']					=	$data[0]->field2;
		$info['field3']					=	$data[0]->field3;
		$info['field4']					=	$data[0]->field4;
		$info['field5']					=	$data[0]->field5;
		$info['field6']					=	$data[0]->field6;
		$info['field7']					=	$data[0]->field7;
		$info['field8']					=	$data[0]->field8;
		$info['field9']					=	$data[0]->field9;
		
		$info['lid']					=	$data[0]->lid;
		$info['listingid']				=	$data[0]->listingid;
		$info['rating']					=	$data[0]->rating;
		$info['services']				=	$data[0]->services;
		$info['primarycategory']		=	$data[0]->primarycategory;
		$info['categories']				=	$data[0]->categories;
		$info['street']					=	$data[0]->street;
		$info['accreditations']			=	$data[0]->accreditations;
		$info['adimage']				=	$data[0]->adimage;
		$info['adimageclick']			=	$data[0]->adimageclick;
		$info['additionaltexts']		=	$data[0]->additionaltexts;
		$info['afterhoursphone']		=	$data[0]->afterhoursphone;
		$info['amenities']				=	$data[0]->amenities;
		$info['associations']			=	$data[0]->associations;
		$info['attribution']			=	$data[0]->attribution;
		$info['attributionlogo']		=	$data[0]->attributionlogo;
		$info['averagerating']			=	$data[0]->averagerating;
		$info['baseclickurl']			=	$data[0]->baseclickurl;
		$info['businessname']			=	$data[0]->businessname;
		$info['certification']			=	$data[0]->certification;
		$info['description1']			=	$data[0]->description1;
		$info['description2']			=	$data[0]->description2;
		$info['detailedhours']			=	explode(',',chunk_split($data[0]->detailedhours,9,','));
		$info['emergencyphone']			=	$data[0]->emergencyphone;
		$info['extraemails']			=	$data[0]->extraemails;
		$info['extrafax']				=	$data[0]->extrafax;
		$info['extraphone']				=	$data[0]->extraphone;
		$info['extratollfree']			=	$data[0]->extratollfree;
		$info['extrawebsiteurls']		=	$data[0]->extrawebsiteurls;
		$info['socialnetworks']			=	$data[0]->socialnetworks;
		$info['generalinfo']			=	$data[0]->generalinfo;
		$info['inbusinesssince']		=	$data[0]->inbusinesssince;
		$info['languagesspoken']		=	$data[0]->languagesspoken;
		$info['locationdescription']	=	$data[0]->locationdescription;
		$info['mobilephone']			=	$data[0]->mobilephone;
		$info['moreinfourl']			=	$data[0]->moreinfourl;
		$info['paymentmethods']			=	$data[0]->paymentmethods;
		$info['ratingcount']			=	$data[0]->ratingcount;
		$info['slogan']					=	$data[0]->slogan;
		$info['videourl']				=	$data[0]->videourl;
		$info['viewphone']				=	$data[0]->viewphone;
		$info['websiteurl']				=	$data[0]->websiteurl;
		
		return $info;
		
	}
	
	public function reviewrating($info){
		$rate = Sdba::table('iweb_hs_ratings',$this->searchads);
		$rating = array('listingidfk'=>$info['listingid'],'uidfk'=>$info['uidfk'],'score'=>$info['score']);
		$rate->insert($rating);
		
		$review = Sdba::table('iweb_hs_reviews',$this->searchads);
		$reviews = array('review_text'=>$info['review_text'],'id'=>$info['listingid'],'uidfk'=>$info['uidfk']);
		$review->insert($reviews);
		
		return 'ok';
	}
	
	public function getlistingtotal($properties){
		//return $properties->totalAvailable;
		return $properties->hitcount;
	}
	
	public function makebuttonaddlisting($categoryname){
		
		$categorysimilar = trim(urldecode(str_replace(' and ',' & ',$categoryname)));
		
		$category = Sdba::table('iweb_hs_categories', $this->searchads);
		$category->where('category_name',$categorysimilar);
		$categoryinfo = $category->get_one();
		
		if(!empty($categoryinfo)){
			$catid = $categoryinfo['category_id'];
			
			$html = '<div class="add-listing-tocategory claim-company">
				<a href="https://dev.avideo.com/?section=signup&categorymembership='.$catid.'" class="btn btn-secondary full-width claim-company">
					<span><i class="fa fa-chevron-circle-right"></i>ADD YOUR LISTING TO THIS CATEGORY<br/><small>[ '.urldecode(str_replace(' and ',' & ',$categoryname)).' ]</small></span>
				</a>
			</div>';
			
			return $html;
			
		}else{
			return false;	
		}
	}
	
	function remove_empty($array) {
	  return array_filter($array, '_remove_empty_internal');
	}
	/**
	* locationcontrol
	* Call the Yellow Page API and get the result
	* @since 3.0
	**/
	public function locationcontrol($getdetails){
		$status = $getdetails['geo_location_strict'];
		
		if($status == '0'){
			$locations = "";
			if(!empty($getdetails['geo_zip'])){
				$ziplist = explode(';',$getdetails['geo_zip']);
				
				$zip = Sdba::table('wwbn_demographics_zip');
				$zip->where('postalcodeid',$ziplist[0]);
				$zips = $zip->get_one();
				
				$city = Sdba::table('wwbn_demographics_city');
				$city->where('CityID',$zips['CityIDKey']);
				$cityinfo = $city->get_one();
				
				$county = Sdba::table('wwbn_demographics_county');
				$county->where('CountyID',$cityinfo['CountyIDKey']);
				$countyinfo = $county->get_one();
				
				$state = Sdba::table('wwbn_demographics_region');
				$state->where('regionshortname',$countyinfo['State']);
				$stateinfo = $state->get_one();
				
				$country = Sdba::table('wwbn_demographics_country');
				$country->where('CountryID',$stateinfo['CountryIDKey']);
				$countryinfo = $country->get_one();
				
				$location = $zips['zipcode'].' '.$cityinfo['cityname'].' '.$countyinfo['County'].' '.$stateinfo['regionname'].' '.$countryinfo['countryname'];
			}
			if(!empty($getdetails['geo_city'])){
				$cities = explode(';',$getdetails['geo_city']);
				
				$city = Sdba::table('wwbn_demographics_city');
				$city->where('CityID',$cities[0]);
				$cityinfo = $city->get_one();
				
				$county = Sdba::table('wwbn_demographics_county');
				$county->where('CountyID',$cityinfo['CountyIDKey']);
				$countyinfo = $county->get_one();
				
				$state = Sdba::table('wwbn_demographics_region');
				$state->where('regionshortname',$countyinfo['State']);
				$stateinfo = $state->get_one();
				
				$country = Sdba::table('wwbn_demographics_country');
				$country->where('CountryID',$stateinfo['CountryIDKey']);
				$countryinfo = $country->get_one();
				
				$location = $cityinfo['cityname'].' '.$countyinfo['County'].' '.$stateinfo['regionname'].' '.$countryinfo['countryname'];
			}
			if(!empty($getdetails['geo_county'])){
				$countylist = explode(';',$getdetails['geo_county']);
				
				$county = Sdba::table('wwbn_demographics_county');
				$county->where('CountyID',$countylist[0]);
				$countyinfo = $county->get_one();
				
				$state = Sdba::table('wwbn_demographics_region');
				$state->where('regionshortname',$countyinfo['State']);
				$stateinfo = $state->get_one();
				
				$country = Sdba::table('wwbn_demographics_country');
				$country->where('CountryID',$stateinfo['CountryIDKey']);
				$countryinfo = $country->get_one();
				
				$location =  $countyinfo['County'].' '.$stateinfo['regionname'].' '.$countryinfo['countryname'];
			}
			if(!empty($getdetails['geo_state'])){
				$regionlist = explode(';',$getdetails['geo_state']);
				
				$state = Sdba::table('wwbn_demographics_region');
				$state->where('RegionID',$regionlist[0]);
				$stateinfo = $state->get_one();
					
				$country = Sdba::table('wwbn_demographics_country');
				$country->where('CountryID',$stateinfo['CountryIDKey']);
				$countryinfo = $country->get_one();
					
				$location =  $stateinfo['regionname'].' '.$countryinfo['countryname'];
			}
			if(!empty($getdetails['geo_country'])){
				$countrylist = explode(';',$getdetails['geo_country']);

				$country = Sdba::table('wwbn_demographics_country');
				$country->where('CountryID',$countrylist[0]);
				$countryinfo = $country->get_one();
					
				$location =  $countryinfo['countryname'];
			}
			
		}else{
			$location = explode(':',$getdetails['strictlocation']);
			$geocolumn = $location[0];
			$geoid = $location[1];
			
			switch($geocolumn){
				case'zip':
					$zip = Sdba::table('wwbn_demographics_zip');
					$zip->where('postalcodeid',$geoid);
					$zips = $zip->get_one();
					
					$city = Sdba::table('wwbn_demographics_city');
					$city->where('CityID',$zips['CityIDKey']);
					$cityinfo = $city->get_one();
					
					$county = Sdba::table('wwbn_demographics_county');
					$county->where('CountyID',$cityinfo['CountyIDKey']);
					$countyinfo = $county->get_one();
					
					$state = Sdba::table('wwbn_demographics_region');
					$state->where('regionshortname',$countyinfo['State']);
					$stateinfo = $state->get_one();
					
					$country = Sdba::table('wwbn_demographics_country');
					$country->where('CountryID',$stateinfo['CountryIDKey']);
					$countryinfo = $country->get_one();
					
					$location =  $zips['zipcode'].' '.$cityinfo['cityname'].' '.$countyinfo['County'].' '.$stateinfo['regionname'].' '.$countryinfo['countryname'];
				break;
				case'city':
		
					$city = Sdba::table('wwbn_demographics_city');
					$city->where('CityID',$geoid);
					$cityinfo = $city->get_one();
					
					$county = Sdba::table('wwbn_demographics_county');
					$county->where('CountyID',$cityinfo['CountyIDKey']);
					$countyinfo = $county->get_one();
					
					$state = Sdba::table('wwbn_demographics_region');
					$state->where('regionshortname',$countyinfo['State']);
					$stateinfo = $state->get_one();
					
					$country = Sdba::table('wwbn_demographics_country');
					$country->where('CountryID',$stateinfo['CountryIDKey']);
					$countryinfo = $country->get_one();
					
					$location =  $cityinfo['cityname'].' '.$countyinfo['County'].' '.$stateinfo['regionname'].' '.$countryinfo['countryname'];
				break;
				case'county':
					$county = Sdba::table('wwbn_demographics_county');
					$county->where('CountyID',$geoid);
					$countyinfo = $county->get_one();
					
					$state = Sdba::table('wwbn_demographics_region');
					$state->where('regionshortname',$countyinfo['State']);
					$stateinfo = $state->get_one();
					
					$country = Sdba::table('wwbn_demographics_country');
					$country->where('CountryID',$stateinfo['CountryIDKey']);
					$countryinfo = $country->get_one();
					
					$location =  $countyinfo['County'].' '.$stateinfo['regionname'].' '.$countryinfo['countryname'];
				break;
				case'state':
					$state = Sdba::table('wwbn_demographics_region');
					$state->where('RegionID',$geoid);
					$stateinfo = $state->get_one();
					
					$country = Sdba::table('wwbn_demographics_country');
					$country->where('CountryID',$stateinfo['CountryIDKey']);
					$countryinfo = $country->get_one();
					
					$location =  $stateinfo['regionname'].' '.$countryinfo['countryname'];
				break;
				case'country':
					$country = Sdba::table('wwbn_demographics_country');
					$country->where('CountryID',$geoid);
					$countryinfo = $country->get_one();
					
					$location =  $countryinfo['countryname'];
				break;
			}
		}
		
		return $location;
	}
	
	public function ypgetlistingdetails($listingid='13519',$network='YP'){
		
		define('APIURL','http://pubapi.yp.com/search-api/search/devapi/details?listingid='.$listingid.'&key=2mw1yc8fzs&format=json');
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,APIURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$data = curl_exec($ch);
		curl_close($ch);
				
		$datadecoded = json_decode($data);
		
		$detail = $datadecoded->listingsDetailsResult->listingsDetails->listingDetail;
		
		$info['accreditations'] 	= 	$detail[0]->accreditations;
		$info['adimage']	 		=  	$detail[0]->adImage;
		$info['adimageclick']	 	=  	$detail[0]->adImageClick;
		$info['additionaltexts']	=  	$detail[0]->adImage;
		$info['afterhoursphone']	=  	$detail[0]->afterHoursPhone;
		$info['amenities']			=  	implode(',',$detail[0]->amenities->amenity);
		$info['associations']		=  	$detail[0]->associations;
		$info['attribution'] 		=	$detail[0]->attribution;
		$info['attributionlogo']	=  	$detail[0]->attributionLogo;
		$info['averagerating']	 	=  	$detail[0]->averageRating;
		$info['baseclickurl']		=  	$detail[0]->baseClickURL;
		$info['businessname']		=  	$detail[0]->businessName;
		$info['categories']			=  	implode(',',$detail[0]->categories->category);
		$info['certification']		=  	$detail[0]->certification;
		$info['city']		 		=	$detail[0]->city;
		$info['description1']		=  	$detail[0]->description1;
		$info['description2']	 	=  	$detail[0]->description2;
		$info['detailedhours']		=  	$detail[0]->detailedHours->defaultHours->standardHours;
		$info['email']				=  	$detail[0]->email;
		$info['emergencyphone']		=  	$detail[0]->emergencyPhone;
		$info['extraemails']		=  	$detail[0]->extraEmails;
		$info['extrafax']		 	=	$detail[0]->extraFax;
		$info['extraphone']			=  	$detail[0]->extraPhone;
		$info['extratollfree']	 	=  	$detail[0]->extraTollFree;
		$info['extrawebsiteurls']	=  	$detail[0]->extraWebsiteURLs->extraWebsiteURL;
		$info['socialnetworks']		=  	implode(',',$detail[0]->features->socialNetworks->socialNetwork);
		$info['generalinfo']		=  	$detail[0]->generalInfo;
		$info['inbusinesssince']	=  	$detail[0]->inBusinessSince;
		$info['languagesspoken']	=  	$detail[0]->languagesSpoken;
		$info['latitude']	 		=  	$detail[0]->latitude;
		$info['longitude']			=  	$detail[0]->longitude;
		$info['locationdescription']=  	$detail[0]->locationDescription;
		$info['mobilephone']		=  	$detail[0]->mobilePhone;
		$info['moreinfourl']		=  	$detail[0]->moreInfoURL;
		$info['openhours']			=  	$detail[0]->openHours;
		$info['paymentmethods']	 	=  	$detail[0]->paymentMethods;
		$info['phone']				=  	$detail[0]->phone;
		$info['primarycategory']	=  	$detail[0]->primaryCategory;
		$info['ratingcount']		=  	$detail[0]->ratingCount;
		$info['services']			=  	$detail[0]->services;
		$info['slogan']				=  	$detail[0]->slogan;
		$info['state']	 			=  	$detail[0]->state;
		$info['street']				=  	$detail[0]->street;
		$info['videourl']			=  	$detail[0]->videoURL;
		$info['viewphone']			=  	$detail[0]->viewPhone;
		$info['websiteurl']			=  	$detail[0]->websiteURL;
		$info['zip']				=  	$detail[0]->zip;
		
		return $info;
	}
	
	public function createmarkersolo($detail){
		//Declaration of Marker for the Vendor.Directory Website
		$markersolo[0]['latitude'] = $detail['latitude'];
		$markersolo[0]['longitude'] = $detail['longitude'];
		$markersolo[0]['icon'] = '/assets/99/img/map-marker.png';
		$markersolo[0]['html'] = htmlentities($detail['businessname'], ENT_QUOTES);
		$markers = json_encode($markersolo);
		
		return $markers;	
	}
	
	public function avideogetlistingdetails($listingid='13519',$network='YP'){
		
		if($network == 'YP'){
			$apiurl = 'http://APIDOMAINCONFIG/index/action::detail/source::YP/lid::'.trim($listingid).'/cb::AVIDEO.com/theme::AVIDEO.com/fmt::json';
		}else{
			$apiurl = 'http://APIDOMAINCONFIG/index/action::detail/source::'.trim($network).'/id::'.$listingid.'/cb::AVIDEO.com/theme::AVIDEO.com/fmt::json';
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$apiurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$data = curl_exec($ch);
		curl_close($ch);
		
		#$cleaned = stripslashes( trim($data) );
		$converted = json_decode($data);
		
		return $converted;
	}

	public function createcompanymarker($detail){
		$markervendor[0]['latitude'] = $detail[0]->latitude;
		$markervendor[0]['longitude'] = $detail[0]->longitude;
		$markervendor[0]['icon'] = '/assets/common/images/map-marker.png';
		$markervendor[0]['html'] = htmlentities($detail[0]->businessName, ENT_QUOTES);
		$markervendor[0]['id'] = 'company-marker';
		
		return json_encode($markervendor);	
	}
	
	public function createdropdowncountries(){
		$country = Sdba::table('wwbn_demographics_country');
		$countriesinfo = $country->get();
		
		$option = "";							
		foreach($countriesinfo as $countrylist => $countryinfo){
			extract($countryinfo);
			$option .= '<option value="'.$countryname.'">'.$countryname.'</option>';	
		}
		
		return $option;
	}
	
	public function createdropdowncities(){
		$cities = Sdba::table('wwbn_demographics_city');
		$cities->where('CountyIDKey','1');
		$citiesinfo = $cities->get();
		
		$option = "";							
		foreach($citiesinfo as $citylist => $cityinfo){
			extract($cityinfo);
			$option .= '<option value="'.$cityname.'">'.$cityname.'</option>';	
		}
		
		return $option;
	}
	
	public function createdropdownregion(){
		$region = Sdba::table('wwbn_demographics_region');
		$region->where('CountryIDKey','1');
		$regioninfo = $region->get();
		
		$option = "";							
		foreach($regioninfo as $regionlist => $regioninfo){
			extract($regioninfo);
			$option .= '<option value="'.$regionname.'">'.$regionname.'</option>';	
		}
		
		return $option;
	}
	
	public function getdomain($url){

		$domainLive = $url;
		
		#if ($_SERVER['HTTP_HOST'] == 'dev.avideo.com'){ $domainLive = "avideo.com"; }
		#if ($_SERVER['HTTP_HOST'] == 'dev.vendor.directory'){ $domainLive = "vendor.directory"; }
		
		if(isset($domainLive)){
			
			$urlExp = explode('.',$domainLive);
			
			if($urlExp[0]=='www'){ //Check if www is available
				
				$mainDomain = str_replace('www.','',$domainLive);
				$nonLLD = count(explode('.',$mainDomain));//echo '<br/>';
				
				if($nonLLD>2){ //If there is a sub domain base on count
					$domainPart = explode('.',$mainDomain); //Explode to get the sub domain
					$subDomain = $domainPart[0]; //Sub Domain
					$domain = str_replace($subDomain.'.','',$mainDomain); //Convert the domain - Add www
					
					$db_domain = Sdba::table('domain_names',$this->domainsite);
					$db_domain->where('domain', $domain);
					$item_domain = $db_domain->get_one(); // loads 1 article
					extract($item_domain);
		
					return json_encode(array('domainid'=>$domainid,'subdomain'=>$subDomain));
		
				}else{ //If there is no sub domain base on count
					$mainDomain = str_replace('www.','',$domainLive);
					$domain = $mainDomain; //Convert the domain - Add www
					
					$db_domain = Sdba::table('domain_names',$this->domainsite);
					$db_domain->where('domain', $domain);
					$item_domain = $db_domain->get_one(); // loads 1 article
					extract($item_domain);
		
					return json_encode(array('domainid'=>$domainid,'subdomain'=>NULL));
				}
				
			}else{
				
				$nonLLD = count(explode('.',$domainLive));
				
				if($nonLLD>2){ //If there is a sub domain base on count
					$domainPart = explode('.',$domainLive); //Explode to get the sub domain
					$subDomain = $domainPart[0]; //echo '<br/>'; //Sub Domain
					unset($domainPart[0]); //Remove the sub domain
					$domain = implode('.',$domainPart); //Create the domain without sub
									
					$db_domain_subfound = Sdba::table('domain_names',$this->domainsite);
					$db_domain_subfound->where('domain', $domain);
					$item_domain_found = $db_domain_subfound->get_one(); // loads 1 article
					extract($item_domain_found);
		
					return json_encode(array('domainid'=>$domainid,'subdomain'=>$subDomain));

				}else{ //If there is no subdomain base on count
					$domainLive; //Convert the domain
					
					$db_domain_subfound = Sdba::table('domain_names',$this->domainsite);
					$db_domain_subfound->where('domain', $domainLive);
					$item_domain_found = $db_domain_subfound->get_one(); // loads 1 article
					extract($item_domain_found);
		
					return json_encode(array('domainid'=>$domainid,'subdomain'=>$subDomain));
				}
				
			}
			
		}
		
	}
	
	public function getsitesconnect($domainsub){
		$domaindecode = json_decode($domainsub);
		$domainid = $domaindecode->domainid;
		$subdomain = $domaindecode->subdomain;
		
		if($subdomain != NULL){
			$domain = Sdba::table('domain_names_sites',$this->domainsite);
			$domain->reset();
			$domain->where('DomainIdKey',$domainid)->and_where('subdomainname',$subdomain);
			$domaininfo = $domain->get_one();
			
			$array = array('domainsiteid'=>$domaininfo['DomainSiteLinkId'],'siteidkey'=>$domaininfo['siteidkey']);
			$json = json_encode($array);
		}else{
			$domain = Sdba::table('domain_names_sites',$this->domainsite);
			$domain->reset();
			$domain->where('DomainIdKey',$domainid);
			$domaininfo = $domain->get_one();
			
			$array = array('domainsiteid'=>$domaininfo['DomainSiteLinkId'],'siteidkey'=>$domaininfo['siteidkey']);
			$json = json_encode($array);
		}
		
		return $json;
	}
	
	public function getdetails($domainsitelinkid){
		$details = Sdba::table('domain_names_sites_details',$this->domainsite);
		$details->where('domainsitelinkidkey',$domainsitelinkid);
		$detailsinfo = $details->get_one();
		
		return $detailsinfo;
	}
	
	public function createplayerembed($videourl){
		if(!empty($videourl)){
			$player = '<div class="video-wrapper-main"><iframe width="560" height="349" src="'.$videourl.'" frameborder="0" allowfullscreen></iframe></div>';	
		}else{
			$player = '<p><i class="fa fa-exclamation-triangle"></i>Ooops! There were no video found for this listing.</p>';
		}	
		
		return $player;
	}
}

?>
